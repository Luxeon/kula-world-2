Kula World 2 is a remake of the puzzle video game "Kula World" released on PlayStation 1 in 1998. The goal is to solve different 3-dimensional mazes by collecting the different keys within a given time.

This is a group project carried out in **Unity** with **C#** language during my 2nd year of study. The project is available for testing purpose on my portfolio just right [here][1].

How to use it ?
---------------

* You can launch the game with the executable files depending of your OS (MasOS not tested) or test it in you Web browser [here][1].
* Once launched, you can choose to start a new game, continue a previously saved game or configure the levels.
* In the configurator, you can load an existing level and modify its structure, the items and also the time limit to complete the level.
* In a game, the controls are the following:
    * ZQD (for AZERTY keyboard), QWD (for QWERTY keyboard) or the arrows to move the ball. You can't go backward, if you need to, you have to turn on yourself.
    * Space to jump. You have to combine jump and move forward to jump over holes or ennemies.
    * Escape (executable files) or M (Web browser) to open the pause menu.

Rules
-----

* The goal of Kula World is to finish each level by collecting the needed keys to open the exit with the biggest score
* In each level you must collect all the keys et get to the exit before the end of the timer
* You can only move forward and turn on yourself. Sometimes, you will need to climb the walls or go down. It is possible only if there is no cube on the left and on the right of the cube your are standing on. You can try and test to understand the mechanic
* You will encounter some items during your game:
    * **Coins and Dollar bills**: they add points to your score
    * **Keys**: you need them to unlock the exit of the current level. You can see how many you need on the bottom left corner
    * **Cristals**: every 5 levels, you can unlock a bonus level only if you collect all the 5 crystals, one by level
    * **Pills**: there are 2 types of pills:
        * *the good one* in blue: it makes you faster and jump higher 
        * *the bad one* in red: it makes you slower 
    * **Hourglasses**: they invert the timer: your elapsed time became your remaining time. Example: you have 30s to complete the level and you take an hourglass when it remains only 5s. It inverts the time so your new remaining time is 25s.
    * **Sunglasses**: some blocks can be invisible, so you need these sunglasses to see them for some seconds
* There are also some ennemies that you don't want to touch:
    * **Laser**: each laser have a color corresponding switch somewhere on the level to activate/deactivate it
    * **Captivator**: this little purple sort of start move on the level
    * **Static and Retracting Spikes**: no need to explain more, just don't touch them
* The score is very important in this game. On the top right corner, you can see your global score alongside your level score. At the end of each level, your level score is added to your global score. If you loose a level (fall into the void, ennemies, times up), you loose 10 points + your current level score. If your global score reaches 0, it is game over and you have to retry from your last checkpoint
* You can save your progression every 5 levels
* In bonus level, you need to activate all the blocks to succeed by going on them


Installation
------------

It is possible for you to test the application by yourself locally. For this, the source files are at your disposal.

About me
--------

I am a Computer Science Master student. You can find some of my other projects on my [Portfolio][2]. 

[1]: https://kulaworld.cyrilpiejougeac.dev
[2]: https://portfolio.cyrilpiejougeac.dev
