﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelData {

    public float[] startPosition;
    public float[] endPosition;

    public float[][][] blocks;

    public float[][][] items;

    public float[][][] spikes;
    public float[][][] captivators;
    public float[][][] lasers;
    public float[][] laserSwitches;

    public int time;
    public int penalty;

    public LevelData(GameObject start, GameObject end, GameObject[][] blocksArray, GameObject[][] itemsArray, GameObject[][] spikesArray, GameObject[] captivatorsArray, GameObject[] lasersArray, GameObject[] laserSwitchesArray, int t, int p)
    {
        startPosition = new float[7];
        startPosition[0] = start.transform.position.x;
        startPosition[1] = start.transform.position.y;
        startPosition[2] = start.transform.position.z;
        startPosition[3] = start.transform.rotation.x;
        startPosition[4] = start.transform.rotation.y;
        startPosition[5] = start.transform.rotation.z;
        startPosition[6] = start.transform.rotation.w;

        if (end != null)
        {
            endPosition = new float[7];
            endPosition[0] = end.transform.position.x;
            endPosition[1] = end.transform.position.y;
            endPosition[2] = end.transform.position.z;
            endPosition[3] = end.transform.rotation.x;
            endPosition[4] = end.transform.rotation.y;
            endPosition[5] = end.transform.rotation.z;
            endPosition[6] = end.transform.rotation.w;
        }

        blocks = new float[blocksArray.Length][][];
        for (int i = 0; i < blocksArray.Length; i++)
        {
            blocks[i] = new float[blocksArray[i].Length][];
            for (int j = 0; j < blocksArray[i].Length; j++)
            {
                blocks[i][j] = new float[3];
                blocks[i][j][0] = blocksArray[i][j].transform.position.x;
                blocks[i][j][1] = blocksArray[i][j].transform.position.y;
                blocks[i][j][2] = blocksArray[i][j].transform.position.z;
            }
        }

        items = new float[itemsArray.Length][][];
        for (int i = 0; i < itemsArray.Length; i++)
        {
            items[i] = new float[itemsArray[i].Length][];
            for (int j = 0; j < itemsArray[i].Length; j++)
            {
                items[i][j] = new float[7];
                items[i][j][0] = itemsArray[i][j].transform.position.x;
                items[i][j][1] = itemsArray[i][j].transform.position.y;
                items[i][j][2] = itemsArray[i][j].transform.position.z;
                items[i][j][3] = itemsArray[i][j].transform.rotation.x;
                items[i][j][4] = itemsArray[i][j].transform.rotation.y;
                items[i][j][5] = itemsArray[i][j].transform.rotation.z;
                items[i][j][6] = itemsArray[i][j].transform.rotation.w;
            }
        }

        spikes = new float[spikesArray.Length][][];
        for (int i = 0; i < spikesArray.Length; i++)
        {
            spikes[i] = new float[spikesArray[i].Length][];
            for (int j = 0; j < spikesArray[i].Length; j++)
            {
                spikes[i][j] = new float[7];
                spikes[i][j][0] = spikesArray[i][j].transform.position.x;
                spikes[i][j][1] = spikesArray[i][j].transform.position.y;
                spikes[i][j][2] = spikesArray[i][j].transform.position.z;
                spikes[i][j][3] = spikesArray[i][j].transform.rotation.x;
                spikes[i][j][4] = spikesArray[i][j].transform.rotation.y;
                spikes[i][j][5] = spikesArray[i][j].transform.rotation.z;
                spikes[i][j][6] = spikesArray[i][j].transform.rotation.w;
            }
        }

        captivators = new float[captivatorsArray.Length][][];
        for (int i = 0; i < captivatorsArray.Length; i++)
        {
            captivators[i] = new float[2][];

            captivators[i][0] = new float[3];
            GameObject point1 = captivatorsArray[i].GetComponentInChildren<Captivator>().point1;
            captivators[i][0][0] = point1.transform.position.x;
            captivators[i][0][1] = point1.transform.position.y;
            captivators[i][0][2] = point1.transform.position.z;

            captivators[i][1] = new float[3];
            GameObject point2 = captivatorsArray[i].GetComponentInChildren<Captivator>().point2;
            captivators[i][1][0] = point2.transform.position.x;
            captivators[i][1][1] = point2.transform.position.y;
            captivators[i][1][2] = point2.transform.position.z;
        }

        lasers = new float[lasersArray.Length][][];
        for (int i = 0; i < lasersArray.Length; i++)
        {
            lasers[i] = new float[3][];

            lasers[i][0] = new float[7];
            GameObject point1 = lasersArray[i].GetComponent<Laser>().point1;
            lasers[i][0][0] = point1.transform.position.x;
            lasers[i][0][1] = point1.transform.position.y;
            lasers[i][0][2] = point1.transform.position.z;
            lasers[i][0][3] = lasersArray[i].transform.rotation.x;
            lasers[i][0][4] = lasersArray[i].transform.rotation.y;
            lasers[i][0][5] = lasersArray[i].transform.rotation.z;
            lasers[i][0][6] = lasersArray[i].transform.rotation.w;

            lasers[i][1] = new float[3];
            GameObject point2 = lasersArray[i].GetComponent<Laser>().point2;
            lasers[i][1][0] = point2.transform.position.x;
            lasers[i][1][1] = point2.transform.position.y;
            lasers[i][1][2] = point2.transform.position.z;

            lasers[i][2] = new float[1];
            lasers[i][2][0] = lasersArray[i].GetComponent<Laser>().id;
        }

        laserSwitches = new float[laserSwitchesArray.Length][];
        for (int i = 0; i < laserSwitchesArray.Length; i++)
        {
            laserSwitches[i] = new float[9];
            laserSwitches[i][0] = laserSwitchesArray[i].transform.position.x;
            laserSwitches[i][1] = laserSwitchesArray[i].transform.position.y;
            laserSwitches[i][2] = laserSwitchesArray[i].transform.position.z;
            laserSwitches[i][3] = laserSwitchesArray[i].transform.rotation.x;
            laserSwitches[i][4] = laserSwitchesArray[i].transform.rotation.y;
            laserSwitches[i][5] = laserSwitchesArray[i].transform.rotation.z;
            laserSwitches[i][6] = laserSwitchesArray[i].transform.rotation.w;
            laserSwitches[i][7] = laserSwitchesArray[i].GetComponent<LaserSwitch>().laserId;
            laserSwitches[i][8] = (laserSwitchesArray[i].GetComponent<LaserSwitch>().activated) ? 1 : 0;
        }

        time = t;

        penalty = p;
    }
}
