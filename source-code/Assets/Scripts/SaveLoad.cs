﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.UI;

public class SaveLoad : MonoBehaviour {

    public GameObject startPrefab;
    public GameObject endPrefab;
    public GameObject[] blocksPrefab;
    public GameObject[] itemsPrefab;
    public GameObject[] fruitsPrefab;
    public GameObject[] spikesPrefab;
    public GameObject captivatorPrefab;
    public GameObject laserPrefab;
    public GameObject laserSwitchPrefab;

    public GameObject scoreEntryPrefab;
    public Transform scoreGrid;

    public static void SaveLevel(int id, int time, int penalty, bool bonus)
    {
        Debug.Log("Level " + id + " Saving Started !");

        string folder;

        if (bonus)
        {
            folder = "/BonusLevels";
        }
        else
        {
            folder = "/StandardLevels";
        }

        // Checking if directory existing and creating it
        if (!Directory.Exists(Application.persistentDataPath + "/Levels" + folder))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Levels" + folder);
        }

        BinaryFormatter formatter = new BinaryFormatter();
        string savePath = Application.persistentDataPath + "/Levels" + folder + "/level" + id + ".bin";
        Debug.Log("Save Path: " + savePath);
        FileStream stream = new FileStream(savePath, FileMode.Create);

        // Get start position
        Debug.Log("Fetching Start Position...");
        GameObject start = GameObject.FindGameObjectWithTag("Start");
        if (start == null)
            Debug.LogError("ERROR: No Start Position Found !");
        else
            Debug.Log("Start Position Fetched !");

        // Get end position
        Debug.Log("Fetching End Position...");
        GameObject end = GameObject.FindGameObjectWithTag("End");
        if (end == null && !bonus)
            Debug.LogError("ERROR: No End Position Found");
        else
            Debug.Log("End Position Fetched !");

        // Get blocks position
        Debug.Log("Fetching Blocks GameObjects...");

        Debug.Log("Fetching Standard Blocks...");
        GameObject[] standardBlocks = GameObject.FindGameObjectsWithTag("Block");
        Debug.Log("Standard Blocks Fetched: " + standardBlocks.Length);

        Debug.Log("Fetching Invisible Blocks...");
        GameObject[] invisibleBlocks = GameObject.FindGameObjectsWithTag("InvisibleBlock");
        Debug.Log("Invisible Blocks Fetched: " + invisibleBlocks.Length);

        Debug.Log("Fetching Flimsy Blocks...");
        GameObject[] flimsyBlocks = GameObject.FindGameObjectsWithTag("FlimsyBlock");
        Debug.Log("Flimsy Blocks Fetched: " + flimsyBlocks.Length);

        Debug.Log("Fetching Bonus Blocks...");
        GameObject[] bonusBlocks = GameObject.FindGameObjectsWithTag("BonusBlock");
        Debug.Log("Invisible Bonus Fetched: " + bonusBlocks.Length);

        GameObject[][] blocks = new GameObject[][] { standardBlocks, invisibleBlocks, flimsyBlocks, bonusBlocks };

        Debug.Log("Blocks GameObjects Fetched !");

        // Get items position
        Debug.Log("Fetching Items GameObjects...");

        Debug.Log("Fetching Keys...");
        GameObject[] keys = GameObject.FindGameObjectsWithTag("Key");
        if (keys.Length == 0 && !bonus)
            Debug.LogError("ERROR: No Keys Found !");
        else
            Debug.Log("Keys Fetched: " + keys.Length);

        Debug.Log("Fetching Hourglasses...");
        GameObject[] hourglasses = GameObject.FindGameObjectsWithTag("Hourglass");
        Debug.Log("Hourglasses Fetched: " + hourglasses.Length);

        Debug.Log("Fetching Fruits...");
        GameObject[] fruits = GameObject.FindGameObjectsWithTag("Fruit");
        if (fruits.Length == 0 && !bonus)
            Debug.LogError("ERROR: No Fruits Found !");
        else
            Debug.Log("Fruits Fetched: " + fruits.Length);

        Debug.Log("Fetching Coins...");
        GameObject[] coins = GameObject.FindGameObjectsWithTag("Coin");
        Debug.Log("Coins Fetched: " + coins.Length);

        Debug.Log("Fetching Gems...");
        GameObject[] gems = GameObject.FindGameObjectsWithTag("Gem");
        Debug.Log("Gems Fetched: " + gems.Length);

        Debug.Log("Fetching LethargyPills...");
        GameObject[] lethargyPills = GameObject.FindGameObjectsWithTag("LethargyPill");
        Debug.Log("LethargyPills Fetched: " + lethargyPills.Length);

        Debug.Log("Fetching BouncyPills...");
        GameObject[] bouncyPills = GameObject.FindGameObjectsWithTag("BouncyPill");
        Debug.Log("BouncyPills Fetched: " + bouncyPills.Length);

        Debug.Log("Fetching Sunglasses...");
        GameObject[] sunglasses = GameObject.FindGameObjectsWithTag("Sunglasses");
        Debug.Log("Sunglasses Fetched: " + sunglasses.Length);

        GameObject[][] items = new GameObject[][] { keys, hourglasses, fruits, coins, gems, lethargyPills, bouncyPills, sunglasses };

        Debug.Log("Items GameObjects Fetched !");

        // Get obstacles positions

        Debug.Log("Fetching Obstacles GameObjects...");

        // Get spikes position
        Debug.Log("Fetching Standard Spikes...");
        GameObject[] standardSpikes = GameObject.FindGameObjectsWithTag("StandardSpikes");
        Debug.Log("Standard Spikes Fetched: " + standardSpikes.Length);

        Debug.Log("Fetching Retracting Spikes...");
        GameObject[] retractingSpikes = GameObject.FindGameObjectsWithTag("RetractingSpikes");
        Debug.Log("Retracting Spikes Fetched: " + retractingSpikes.Length);

        GameObject[][] spikes = new GameObject[][] { standardSpikes, retractingSpikes };

        // Get captivators position
        Debug.Log("Fetching Captivators...");
        GameObject[] captivators = GameObject.FindGameObjectsWithTag("Captivator");
        Debug.Log("Captivators Fetched: " + captivators.Length);

        // Get lasers position
        Debug.Log("Fetching Lasers...");
        GameObject[] lasers = GameObject.FindGameObjectsWithTag("Laser");
        Debug.Log("Lasers Fetched: " + lasers.Length);

        // Get laser switches position
        Debug.Log("Fetching Laser Switches...");
        GameObject[] laserSwitches = GameObject.FindGameObjectsWithTag("LaserSwitch");
        Debug.Log("Laser Switches Fetched: " + laserSwitches.Length);

        Debug.Log("Obstacles GameObjects Fetched !");


        Debug.Log("Creating Level Data");
        LevelData data = new LevelData(start, end, blocks, items, spikes, captivators, lasers, laserSwitches, time, penalty);

        formatter.Serialize(stream, data);
        Debug.Log("Level Saving Complete ! Stored in " + savePath);

        stream.Close();
    }
    
    public static LevelData LoadLevelData(int id, bool bonus)
    {
        // Get the folder according to the level type (bonus or not)
        string folder;

        if (bonus)     
        {
            folder = "/BonusLevels";
        }
        else
        {
            folder = "/StandardLevels";
        }
        
        // Set the file path
        string loadPath = Application.persistentDataPath + "/Levels" + folder + "/level" + id + ".bin";
        Debug.Log("Fetching level data in " + loadPath + " file...");

        GameObject defaultLevelPrefab;

        if (bonus)
        {
            defaultLevelPrefab = Resources.Load<GameObject>("Levels/BonusLevels/Level" + id);
        }
        else
        {
            defaultLevelPrefab = Resources.Load<GameObject>("Levels/StandardLevels/Level" + id);
        }

        // Save Default Level in folder if file does not exists
        if (!File.Exists(loadPath) && defaultLevelPrefab != null)
        {
            GameObject tempLevel = Instantiate(defaultLevelPrefab);

            int time = tempLevel.GetComponentInChildren<DefaultLevelInfo>().time;
            int penalty = tempLevel.GetComponentInChildren<DefaultLevelInfo>().penalty;
            Debug.Log(time);

            
            SaveLevel(id, time, penalty, bonus);
            DestroyImmediate(tempLevel);
        }

        // Load the level data and return it
        if (File.Exists(loadPath))     
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(loadPath, FileMode.Open);

            LevelData data = (LevelData) formatter.Deserialize(stream);
            stream.Close();

            Debug.Log("Data fetched !");
            return data;
        }

        // Report error if no level were found and Default Level does not exists
        Debug.LogError("ERROR : No Data Found !");
        return null;
    }

    public void SaveLevelFromEditor(int id, int time, bool bonus)
    {
        if (!bonus)
        {
            SaveLevel(id, time, 20, bonus);
        } else  // Bonus Levels shouldn't have penalty
        {
            SaveLevel(id, time, 0, bonus);
        }
    }

    public void LoadLevel(int id)      // Function that load standard level and the UI
    {
        Debug.Log("Level " + id + " Generation Started !");

        // Get data about the level id
        LevelData levelData = LoadLevelData(id, false);

        // Load level
        LoadBlocks(levelData);
        LoadItems(levelData);
        LoadObstacles(levelData);
        LoadStartPosition(levelData);
        LoadEndPosition(levelData);
        Debug.Log("Setting Level Time...");
        GameManager.instance.startTime = levelData.time;
        Debug.Log("Setting Level Penalty...");
        GameManager.instance.levelPenalty = levelData.penalty;

        // Load UI
        Debug.Log("Loading UI...");

        GameManager.instance.levelTab.SetActive(true);
        GameManager.instance.levelTab.GetComponentInChildren<Text>().text = "Level " + GameManager.currentLevel.ToString();
        RectTransform levelBackground = GameManager.instance.levelTab.transform.Find("Background").GetComponent<RectTransform>();
        levelBackground.sizeDelta = new Vector2(GameManager.instance.levelTab.GetComponentInChildren<Text>().preferredWidth + 107, levelBackground.rect.height);

        GameManager.instance.countdown.gameObject.SetActive(true);

        GameManager.instance.timeTab.SetActive(true);
        GameManager.instance.time = GameManager.instance.startTime;
        GameManager.instance.timeTab.GetComponentInChildren<Text>().text = string.Format("{0:00}", GameManager.instance.time);

        GameManager.instance.scoreTab.SetActive(true);
        GameManager.instance.UpdateScore();
        GameManager.instance.scoreTab.transform.Find("TotalScore").GetComponent<Text>().text = PlayerStats.totalScore.ToString();

        // Display the right number of empty key sprite for the UI
        GameManager.instance.keyTab.SetActive(true);
        int keys = GameObject.FindGameObjectsWithTag("Key").Length;
        GameManager.instance.keyNumberRequired = keys;
        RectTransform keysBackground = GameManager.instance.keyTab.transform.Find("Background").GetComponent<RectTransform>();
        keysBackground.sizeDelta = new Vector2(60 + keys * 83, keysBackground.rect.height);
        Transform keysUiGroup = GameManager.instance.keyTab.transform;
        RectTransform sampleKeySprite = GameObject.Find("SampleKeySprite").GetComponent<RectTransform>();
        for (int i = 0; i < keys; i++)
        {
            GameObject keySprite = Instantiate(GameManager.instance.emptyKeyPrefab, new Vector3(18 + i * 83, 51.3f, 0), Quaternion.identity);
            keySprite.transform.parent = keysUiGroup;
            keySprite.GetComponent<RectTransform>().localScale = sampleKeySprite.localScale;
            keySprite.GetComponent<RectTransform>().localPosition = new Vector2(sampleKeySprite.localPosition.x + i * 83, sampleKeySprite.localPosition.y);
        }

        GameManager.instance.bonusTab.SetActive(true);
        GameManager.instance.startBonus = PlayerStats.bonus;
        for (int i = 0; i < PlayerStats.bonus; i++)
        {
            GameManager.instance.bonusSprites[i].SetActive(true);
        }

        Debug.Log("UI Loaded !");

        Debug.Log("Level " + id + " Generation Ended !");
    }

    public void LoadLevelFromEditor(int id, bool bonus)     // Function that load standard level for Editor
    {
        if (bonus)
        {
            Debug.Log("Bonus Level " + id + " Generation Started !");
        } else
        {
            Debug.Log("Level " + id + " Generation Started !");
        }

        // Get data about the level id
        LevelData levelData = LoadLevelData(id, bonus);

        // Load level
        LoadBlocks(levelData);
        LoadItems(levelData);
        LoadObstacles(levelData);
        LoadStartPosition(levelData);
        if (!bonus)
        {
            LoadEndPosition(levelData);
        }
        Debug.Log("Setting Level Time...");
        GameObject.Find("TimeInputField").GetComponent<InputField>().text = levelData.time.ToString();
    }

    public void LoadBonusLevel(int id)     // Function that load bonus level and the UI according to the bonus
    {
        Debug.Log("Bonus Level " + id + " Generation Started !");

        // Get data about the level id
        LevelData levelData = LoadLevelData(id, true);

        // Load level
        LoadBlocks(levelData);
        LoadItems(levelData);
        LoadObstacles(levelData);
        LoadStartPosition(levelData);
        Debug.Log("Setting Level Time...");
        GameManager.instance.startTime = levelData.time;

        // Load UI
        Debug.Log("Loading UI...");

        GameManager.instance.levelTab.SetActive(true);
        GameManager.instance.levelTab.GetComponentInChildren<Text>().text = "Bonus Level";
        RectTransform levelBackground = GameManager.instance.levelTab.transform.Find("Background").GetComponent<RectTransform>();
        levelBackground.sizeDelta = new Vector2(GameManager.instance.levelTab.GetComponentInChildren<Text>().preferredWidth + 107, levelBackground.rect.height);
        GameManager.instance.levelTab.GetComponentInChildren<Animator>().SetTrigger("activate");

        GameManager.instance.countdown.gameObject.SetActive(true);

        GameManager.instance.timeTab.SetActive(true);
        GameManager.instance.time = GameManager.instance.startTime;
        GameManager.instance.timeTab.GetComponentInChildren<Text>().text = string.Format("{0:00}", GameManager.instance.time);

        GameManager.instance.scoreTab.SetActive(true);
        GameManager.instance.UpdateScore();
        GameManager.instance.scoreTab.transform.Find("TotalScore").GetComponent<Text>().text = PlayerStats.totalScore.ToString();

        GameManager.instance.bonusBlocksTab.SetActive(true);
        GameManager.instance.bonusBlocksRemaining = GameObject.FindGameObjectsWithTag("BonusBlock").Length;
        GameManager.instance.bonusBlocksTab.GetComponentInChildren<Text>().text = "Blocks Remaining: " + GameManager.instance.bonusBlocksRemaining.ToString();
        RectTransform bonusBlocksBackground = GameManager.instance.bonusBlocksTab.transform.Find("Background").GetComponent<RectTransform>();
        bonusBlocksBackground.sizeDelta = new Vector2(GameManager.instance.bonusBlocksTab.GetComponentInChildren<Text>().preferredWidth + 107, bonusBlocksBackground.rect.height);

        Debug.Log("UI Loaded !");

        Debug.Log("Bonus Level " + id + " Generation Ended !");
    }

    public void LoadStartPosition(LevelData level)
    {
        float[] start = level.startPosition;

        // Generate start position
        Debug.Log("Setting Start Position...");

        GameObject startGO;

        if (start.Length < 7)  // Old levels might not have start rotation
        {
            startGO = Instantiate(startPrefab, new Vector3(start[0], start[1], start[2]), Quaternion.identity);
        } else
        {
            startGO = Instantiate(startPrefab, new Vector3(start[0], start[1], start[2]), new Quaternion(start[3], start[4], start[5], start[6]));
        }

        startGO.transform.parent = GameObject.Find("Level").transform;

        Debug.Log("Start Position Set !");
    }

    public void LoadEndPosition(LevelData level)
    {
        float[] end = level.endPosition;

        // Generate end position
        Debug.Log("Setting End Position...");

        GameObject endGO = Instantiate(endPrefab, new Vector3(end[0], end[1], end[2]), new Quaternion(end[3], end[4], end[5], end[6]));
        endGO.transform.parent = GameObject.Find("Level").transform;

        Debug.Log("End Position Set !");
    }

    public void LoadBlocks(LevelData level)
    {
        float[][][] blocks = level.blocks;
        string[] blocksFolders = new string[] { "StandardBlocks", "InvisibleBlocks", "FlimsyBlocks", "BonusBlocks" };

        // Generate blocks
        Debug.Log("Loading Blocks...");

        for (int i = 0; i < blocks.Length; i++)
        {
            for (int j = 0; j < blocks[i].Length; j++)
            {
                GameObject blockGO = Instantiate(blocksPrefab[i], new Vector3(blocks[i][j][0], blocks[i][j][1], blocks[i][j][2]), Quaternion.identity);
                blockGO.transform.parent = GameObject.Find(blocksFolders[i]).transform;
            }
        }

        Debug.Log("Blocks Loaded !");
    }

    public void LoadItems(LevelData level)
    {
        float[][][] items = level.items;
        string[] itemsFolders = new string[] { "Keys", "Hourglasses", "Fruits", "Coins", "Gems", "LethargyPills", "BouncyPills", "Sunglasses" };

        Debug.Log("Loading Items...");

        // Set the fruit prefab depending the number of fruits already obtained by the player
        Debug.Log("Fetch Fruit Prefab n°" + (PlayerStats.bonus + 1) + "...");
        itemsPrefab[2] = fruitsPrefab[PlayerStats.bonus];

        // Generate items
        Debug.Log("Generate Items...");
        for (int i = 0; i < items.Length; i++)
        {
            for (int j = 0; j < items[i].Length; j++)
            {
                GameObject itemGO = Instantiate(itemsPrefab[i], new Vector3(items[i][j][0], items[i][j][1], items[i][j][2]), new Quaternion(items[i][j][3], items[i][j][4], items[i][j][5], items[i][j][6]));
                itemGO.transform.parent = GameObject.Find("Items").transform.Find(itemsFolders[i]);
            }
        }

        Debug.Log("Items Loaded !");
    }

    public void LoadObstacles(LevelData level)
    {
        float[][][] spikes = level.spikes;
        string[] spikesFolders = new string[] { "StandardSpikes", "RetractingSpikes" };

        float[][][] captivators = level.captivators;

        float[][][] lasers = level.lasers;

        float[][] laserSwitches = level.laserSwitches;

        Debug.Log("Loading Obstacles...");

        // Generate spikes
        Debug.Log("Loading Spikes...");
        for (int i = 0; i < spikes.Length; i++)
        {
            for (int j = 0; j < spikes[i].Length; j++)
            {
                GameObject spikesGO = Instantiate(spikesPrefab[i], new Vector3(spikes[i][j][0], spikes[i][j][1], spikes[i][j][2]), new Quaternion(spikes[i][j][3], spikes[i][j][4], spikes[i][j][5], spikes[i][j][6]));
                spikesGO.transform.parent = GameObject.Find(spikesFolders[i]).transform;
            }
        }
        Debug.Log("Spikes Loaded !");

        // Generate captivators
        Debug.Log("Loading Captivators...");
        for (int i = 0; i < captivators.Length; i++)
        {
            GameObject captivatorGO = Instantiate(captivatorPrefab, new Vector3(captivators[i][0][0], captivators[i][0][1], captivators[i][0][2]), Quaternion.identity);

            captivatorGO.GetComponentInChildren<Captivator>().point2.transform.position = new Vector3(captivators[i][1][0], captivators[i][1][1], captivators[i][1][2]);

            captivatorGO.transform.parent = GameObject.Find("Captivators").transform;
        }
        Debug.Log("Captivators Loaded !");

        // Generate lasers
        Debug.Log("Loading Laser...");
        for (int i = 0; i < lasers.Length; i++)
        {
            GameObject laserGO = Instantiate(laserPrefab, new Vector3(lasers[i][0][0], lasers[i][0][1], lasers[i][0][2]), new Quaternion(lasers[i][0][3], lasers[i][0][4], lasers[i][0][5], lasers[i][0][6]));

            laserGO.GetComponent<Laser>().point2.transform.position = new Vector3(lasers[i][1][0], lasers[i][1][1], lasers[i][1][2]);

            laserGO.GetComponent<Laser>().id = (int)lasers[i][2][0];

            laserGO.GetComponent<Laser>().Initialize();

            laserGO.transform.parent = GameObject.Find("Lasers").transform;
        }
        Debug.Log("Laser Loaded !");

        // Generate laser switches
        Debug.Log("Loading Laser Switches...");
        for (int i = 0; i < laserSwitches.Length; i++)
        {
            GameObject laserSwitchGO = Instantiate(laserSwitchPrefab, new Vector3(laserSwitches[i][0], laserSwitches[i][1], laserSwitches[i][2]), new Quaternion(laserSwitches[i][3], laserSwitches[i][4], laserSwitches[i][5], laserSwitches[i][6]));

            laserSwitchGO.GetComponent<LaserSwitch>().laserId = (int)laserSwitches[i][7];

            laserSwitchGO.GetComponent<LaserSwitch>().activated = (laserSwitches[i][8] == 1) ? true : false;

            laserSwitchGO.GetComponent<LaserSwitch>().Initialize();

            laserSwitchGO.transform.parent = GameObject.Find("LaserSwitches").transform;
        }
        Debug.Log("Laser Switches Loaded !");

        Debug.Log("Obstacles Loaded !");
    }

    public static void SaveScore(string username)
    {
        ScoreBoardData data = new ScoreBoardData(username);

        BinaryFormatter formatter = new BinaryFormatter();
        string savePath = Application.persistentDataPath + "/scoreboard.bin";
        FileStream stream = new FileStream(savePath, FileMode.Create);

        formatter.Serialize(stream, data);

        stream.Close();
    }

    public static ScoreBoardData LoadScoreData()
    {
        string loadPath = Application.persistentDataPath + "/scoreboard.bin";

        if (File.Exists(loadPath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(loadPath, FileMode.Open);

            ScoreBoardData data = (ScoreBoardData)formatter.Deserialize(stream);
            stream.Close();

            Debug.Log("Data fetched !");
            return data;
        }
        else
        {
            Debug.LogError("ERROR : No Data Found !");
            return null;
        }
    }

    public void LoadScoreBoard()
    {
        string[][] scoreBoard = LoadScoreData().scoreBoard;

        foreach (string[] score in scoreBoard)
        {
            GameObject entry = Instantiate(scoreEntryPrefab, Vector3.zero, Quaternion.identity);
            entry.transform.SetParent(scoreGrid);

            entry.transform.Find("Rank").GetComponentInChildren<Text>().text = score[0];
            entry.transform.Find("Username").GetComponentInChildren<Text>().text = score[1];
            entry.transform.Find("Score").GetComponentInChildren<Text>().text = score[2];

            entry.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public static GameObject[] GetChildrenOfGameObject(GameObject obj)
    {
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in obj.transform)
        {
            children.Add(child.gameObject);
        }

        return children.ToArray();
    }
}
