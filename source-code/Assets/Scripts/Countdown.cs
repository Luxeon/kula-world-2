﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Countdown : MonoBehaviour
{
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
