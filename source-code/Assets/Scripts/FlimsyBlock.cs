﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlimsyBlock : MonoBehaviour {

    private Vector3 startPosition;
    public bool isFalling;

    void Awake()
    {
        // Load the material according to the current world
        GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blocks/World" + (1 + (GameManager.currentLevel - 1) / 5) + "/FlimsyBlock");
    
        startPosition = transform.position;
    }

	void OnCollisionEnter(Collision collision)
    {
        // Enable gravity when colliding with the player and destroy the block when touching something else when falling
        if (collision.gameObject.tag == "Player" && !isFalling)
        {
            GetComponent<Rigidbody>().useGravity = true;
            isFalling = true;
            Debug.Log(this.name + " is falling...");
        }
        else if (collision.gameObject.tag != "Player" && isFalling)
        {
            Destroy(this.gameObject);
        }
    }

    void Update()
    {
        // Destroy the block when it's 5 block far from its start position
        if (Vector3.Distance(transform.position, startPosition) >= 5)
        {
            Destroy(this.gameObject);
        }
    }
}
