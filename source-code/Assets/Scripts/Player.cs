﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public AudioClip jumpSound;

    public Vector3 axisX;
    public Vector3 axisY;
    public Vector3 axisZ;

    private Rigidbody _rb;

    public float startSpeed = 5f;
    public float _speed;
    [SerializeField]
    private float _gravity;
    private Vector3 _oldGravity;

    [SerializeField]
    private bool _blockFront;
    [SerializeField]
    private bool _blockForward;
    [SerializeField]
    private bool _blockLeft;
    [SerializeField]
    private bool _blockRight;

    [SerializeField]
    private Vector3 _currentBlockPos;
    [SerializeField]
    private float _endPos;

    [SerializeField]
    private bool _isMoving;
    [SerializeField]
    public bool _isRotating;
    [SerializeField]
    private bool _isBlocked;
    [SerializeField]
    public bool _isStopping;
    [SerializeField]
    private bool _isJumping;

    [SerializeField]
    private float _angle;
    [SerializeField]
    private bool _jumpLock = true;
    [SerializeField]
    private float _v0;
    [SerializeField]
    private Vector3 _posBeforeJump;

    private Camera _camera;

    private bool malus = false;
    private bool bonus = false;

    void Awake()
    {
        axisX = transform.right;
        axisY = transform.up;
        axisZ = transform.forward;
    }

    // Use this for initialization
    void Start()
    {
        _speed = startSpeed;

        Physics.gravity = -axisY * _gravity;

        _rb = GetComponent<Rigidbody>();

        RaycastHit bottomHit;

        Physics.Raycast(transform.position, -axisY, out bottomHit, 1f);
        _currentBlockPos = bottomHit.transform.position;

        _camera = GameObject.Find("Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.gameOver || GameManager.instance.inPause)
        {
            return;
        }

        if (!GameManager.instance.gameStarted || GameManager.instance.gameOver)
        {
            return;
        }

        if (Vector3.Scale(transform.position, axisY).magnitude <= 0 || Vector3.Scale(transform.position, axisY).magnitude >= GameManager.instance.border)
        {
            GameManager.instance.GameOver();
        }

        EnvironmentDetection();

        if ((Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.Q)) && !_isMoving && !_isRotating)
        {
            axisX = RotateVector(axisX, axisY, -90);
            axisZ = RotateVector(axisZ, axisY, -90);

            _isRotating = true;

            _camera.StartRotateCameraCoroutine(-90, axisY);
        }
        else if ((Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) && !_isMoving && !_isRotating)
        {
            axisX = RotateVector(axisX, axisY, 90);
            axisZ = RotateVector(axisZ, axisY, 90);

            _isRotating = true;

            _camera.StartRotateCameraCoroutine(90, axisY);
        }
        else if ((Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Z)) && !_isStopping && !_isJumping && !_isRotating)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                // Play sound
                GameObject.Find("Jump").GetComponent<AudioSource>().PlayOneShot(jumpSound);

                _rb.velocity = Vector3.zero;
                _rb.angularVelocity = Vector3.zero;

                _posBeforeJump = transform.position;

                _rb.AddForce(_v0 * Mathf.Cos(Mathf.Deg2Rad * _angle) * axisZ + _v0 * Mathf.Sin(Mathf.Deg2Rad * _angle) * axisY, ForceMode.Impulse);

                _isJumping = true;
                _isMoving = false;
            }
            else if (!_isBlocked)
            {
                _isMoving = true;
            }
            else
            {
                if (_isMoving)
                {
                    _isStopping = true;
                    _endPos = Mathf.RoundToInt(Vector3.Scale(transform.position, axisZ).magnitude + (axisZ.x + axisZ.y + axisZ.z) * 0.5f);
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.Z))
        {
            if (_isMoving)
            {
                _isStopping = true;
                _endPos = Mathf.RoundToInt(Vector3.Scale(transform.position, axisZ).magnitude + (axisZ.x + axisZ.y + axisZ.z) * 0.5f);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && !_isJumping)
        {
            _posBeforeJump = transform.position;
            _rb.AddForce(_v0 * Mathf.Cos(Mathf.Deg2Rad * 90) * axisZ + _v0 * Mathf.Sin(Mathf.Deg2Rad * 90) * axisY, ForceMode.Impulse);
            // Play sound
            GameObject.Find("Jump").GetComponent<AudioSource>().PlayOneShot(jumpSound);
            _isJumping = true;
        }
    }

    void FixedUpdate()
    {
        if (_isMoving)
        {
            RaycastHit nextBlockHit;
            Vector3 direction;

            if (Physics.Raycast(transform.position + 0.9f * axisZ, -axisY, out nextBlockHit, 1f))
            {
                if (nextBlockHit.collider.tag == "Block" || nextBlockHit.collider.tag == "InvisibleBlock" || nextBlockHit.collider.tag == "FlimsyBlock" || nextBlockHit.collider.tag == "BonusBlock")
                {
                    direction = (nextBlockHit.transform.position + 0.75f * axisY - transform.position).normalized;
                }
                else
                {
                    direction = axisZ;
                }
            }
            else
            {
                direction = axisZ;
            }

            _rb.velocity = direction * _speed;

            RaycastHit frontHit;

            if (Physics.Raycast(transform.position, axisZ, out frontHit, 0.251f))
            {
                if (frontHit.collider.tag == "Block" || frontHit.collider.tag == "InvisibleBlock" || frontHit.collider.tag == "FlimsyBlock" || frontHit.collider.tag == "BonusBlock")
                {
                    Physics.gravity = axisZ * _gravity;

                    axisY = RotateVector(axisY, axisX, -90);
                    axisZ = RotateVector(axisZ, axisX, -90);

                    _isRotating = true;

                    _camera.StartRotateCameraCoroutine(-90, axisX);

                    if (_isStopping)
                    {
                        _endPos = Mathf.RoundToInt(Vector3.Scale(transform.position, axisZ).magnitude + (axisZ.x + axisZ.y + axisZ.z) * 0.5f);
                    }
                }
            }

            RaycastHit bottomHit;
            RaycastHit gravityHit;

            if (Physics.Raycast(transform.position, -axisY, out bottomHit, 1f) && (bottomHit.collider.tag == "Block" || bottomHit.collider.tag == "InvisibleBlock" || bottomHit.collider.tag == "FlimsyBlock" || bottomHit.collider.tag == "BonusBlock"))
            {
                _currentBlockPos = bottomHit.transform.position;
                _oldGravity = Physics.gravity;
            }
            else
            {
                Physics.Raycast(transform.position, _currentBlockPos - transform.position, out gravityHit, 1f);

                Physics.gravity = -gravityHit.normal.normalized * _gravity;

                axisY = gravityHit.normal.normalized;
                axisZ = RotateVector(axisY, axisX, 90);

                if (Physics.gravity != _oldGravity)
                {
                    _isRotating = true;

                    _camera.StartRotateCameraCoroutine(90, axisX);
                    _oldGravity = Physics.gravity;
                }

                if (_isStopping)
                {
                    _endPos = Mathf.RoundToInt(Vector3.Scale(transform.position, axisZ).magnitude + (axisZ.x + axisZ.y + axisZ.z) * 0.5f);
                }
            }
        }

        if (_isStopping)
        {
            float pos = Vector3.Scale(transform.position, axisZ).magnitude;
            float distance = Mathf.Abs(pos - _endPos);

            if (distance <= 0.1 * _speed / startSpeed)    // A modifier si changement de la vitesse
            {
                _rb.velocity = Vector3.zero;
                _rb.angularVelocity = Vector3.zero;
                _isMoving = false;

                _isStopping = false;

                transform.position = _currentBlockPos + 0.75f * axisY;
            }
        }

        if (_isJumping)
        {
            RaycastHit bottomHit;
            Debug.DrawRay(_posBeforeJump + (bonus ? 3 : 2) * axisZ - 0.75f * axisY, axisY * 0.49f, Color.red);

            if (Physics.Raycast(transform.position, -axisY, out bottomHit, 0.25f) && _jumpLock == false && CheckIsObjectBlock(bottomHit.transform.gameObject))
            {
                _isJumping = false;
                _jumpLock = true;

                _rb.velocity = Vector3.zero;
                _rb.angularVelocity = Vector3.zero;

                //_currentBlockPos = bottomHit.transform.position;
                //transform.position = _currentBlockPos + 0.75f * axisY;
            }
            else if (!Physics.Raycast(transform.position, -axisY, 0.25f) && _jumpLock == true)
            {
                _jumpLock = false;
            }
            else if (Physics.Raycast(_posBeforeJump + (bonus ? 3 : 2) * axisZ - 0.75f * axisY, axisY, 0.49f))
            {
                _rb.velocity = Vector3.Scale(_rb.velocity, new Vector3(Mathf.Abs(axisX.x + axisY.x), Mathf.Abs(axisX.y + axisY.y), Mathf.Abs(axisX.z + axisY.z)));
            }
        }
    }

    public bool CheckIsObjectBlock(GameObject obj)
    {
        return obj.tag == "Block" || obj.tag == "FlimsyBlock" || obj.tag == "BonusBlock" || obj.tag == "InvisibleBlock";
    }

    public void EnvironmentDetection()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position + axisZ, -axisY, out hit, 0.5f))
        {
            if (hit.collider.tag == "Block" || hit.collider.tag == "InvisibleBlock" || hit.collider.tag == "FlimsyBlock" || hit.collider.tag == "BonusBlock")
            {
                _blockForward = true;
            }
            else
            {
                _blockForward = false;
            }
        }
        else
        {
            _blockForward = false;
        }

        if (Physics.Raycast(transform.position, axisZ, out hit, 0.5f))
        {
            if (hit.collider.tag == "Block" || hit.collider.tag == "InvisibleBlock" || hit.collider.tag == "FlimsyBlock" || hit.collider.tag == "BonusBlock")
            {
                _blockFront = true;
            }
            else
            {
                _blockFront = false;
            }
        }
        else
        {
            _blockFront = false;
        }

        if (Physics.Raycast(transform.position - axisX, -axisY, out hit, 0.5f))
        {
            if (hit.collider.tag == "Block" || hit.collider.tag == "InvisibleBlock" || hit.collider.tag == "FlimsyBlock" || hit.collider.tag == "BonusBlock")
            {
                _blockLeft = true;
            }
            else
            {
                _blockLeft = false;
            }
        }
        else
        {
            _blockLeft = false;
        }

        if (Physics.Raycast(transform.position + axisX, -axisY, out hit, 0.5f))
        {
            if (hit.collider.tag == "Block" || hit.collider.tag == "InvisibleBlock" || hit.collider.tag == "FlimsyBlock" || hit.collider.tag == "BonusBlock")
            {
                _blockRight = true;
            }
            else
            {
                _blockRight = false;
            }
        }
        else
        {
            _blockRight = false;
        }

        if (!_blockFront && !_blockForward && (_blockLeft || _blockRight))
        {
            _isBlocked = true;
        }
        else
        {
            _isBlocked = false;
        }
    }

    public Vector3 RotateVector(Vector3 vect, Vector3 axis, float angle, int toFloat = 0)
    {
        Vector3 tmpVector = Quaternion.Euler(axis * angle) * vect;

        if (toFloat == 0)
        {
            vect = new Vector3(Mathf.RoundToInt(tmpVector.x), Mathf.RoundToInt(tmpVector.y), Mathf.RoundToInt(tmpVector.z));
        }
        else if (toFloat == 1)
        {
            vect = tmpVector;
        }

        return vect;
    }

    public void StartPillMalus()
    {
        if (bonus)
        {
            StartCoroutine(BouncyBehaviour());
        }
        if (!malus)
        {
            StartCoroutine(LethargyBehaviour());
        }
    }

    public void StartPillBonus()
    {
        if (malus)
        {
            StartCoroutine(LethargyBehaviour());
        }
        else if (!bonus)
        {
            StartCoroutine(BouncyBehaviour());
        }
    }

    IEnumerator LethargyBehaviour()
    {
        if (!malus)
        {
            malus = true;
            _speed *= 0.5f;
            GameManager.instance.timeSpeed *= 2f;

            yield return new WaitForSeconds(5f);
        }

        malus = false;
        _speed = startSpeed;
        GameManager.instance.timeSpeed = 1f;
    }

    IEnumerator BouncyBehaviour()
    {
        if (!bonus)
        {
            bonus = true;
            _speed *= 1.5f;
            _v0 = 8.5f;

            yield return new WaitForSeconds(5f);
        }

        bonus = false;
        _speed = startSpeed;
        _v0 = 6.9f;
    }
}
