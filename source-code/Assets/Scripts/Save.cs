﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Save : MonoBehaviour {

	public void Yes()
    {
        Debug.Log("Save: Yes");

        PlayerPrefs.SetInt("Level", GameManager.currentLevel + 1);
        PlayerPrefs.SetInt("TotalScore", PlayerStats.totalScore);
        StartCoroutine(MenuChangeCoroutine());
    }

    public void No()
    {
        Debug.Log("Save: No");

        StartCoroutine(MenuChangeCoroutine());
    }

    IEnumerator MenuChangeCoroutine()
    {
        GameManager.instance.saveCanvas.GetComponent<Animator>().SetBool("active", false);

        yield return new WaitForSeconds(0.92f);

        GameManager.instance.saveCanvas.SetActive(false);
        GameManager.instance.winCanvas.SetActive(true);

        GameManager.instance.winCanvas.GetComponent<Animator>().SetBool("active", true);
    }
}
