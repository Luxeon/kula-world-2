﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour {

    //private EndGate endGate;
    public GameObject particlesPrefab;

    public AudioClip sound;

    void Start()
    {
        //endGate = GameObject.FindGameObjectWithTag("End").GetComponentInChildren<EndGate>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // Play sound
            GameObject.Find("Collectible").GetComponent<AudioSource>().PlayOneShot(sound);

            Debug.Log(this.name + " Picked Up !");

            // Add 1 key to the player stats and check if the end gate can be open
            PlayerStats.keys += 1;

            GameManager.instance.UpdateKeys();

            GameObject[] endGates = GameObject.FindGameObjectsWithTag("End");

            foreach (GameObject endGate in endGates)
            {
                endGate.GetComponentInChildren<EndGate>().CheckIfOpen();
            }

            Destroy(this.gameObject);

            StartCoroutine(InstantiateParticles());
        }
    }

    IEnumerator InstantiateParticles()
    {
        GameObject particles = Instantiate(particlesPrefab, transform.position + 0.85f * transform.up, transform.rotation);

        yield return new WaitForSeconds(1f);

        Destroy(particles);
    }
}
