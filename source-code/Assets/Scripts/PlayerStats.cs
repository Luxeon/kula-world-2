﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

	public static int keys;
    public static int bonus;
    public static int score;
    public static int totalScore;

    void Awake ()
    {
        keys = 0;
        score = 0;
    }

    public static void LogStats()
    {
        Debug.Log("DEBUG MODE: Keys: " + keys + " Bonus: " + bonus + " Score: " + score + " TotalScore: " + totalScore + " LastSavedLevel: " + PlayerPrefs.GetInt("Level"));
    }
}
