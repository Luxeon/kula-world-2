﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public AudioClip stageMusic;
    public AudioClip bonusMusic;

    public AudioClip victorySound;
    public AudioClip gameOverSound;

    public Animator transitionController;

    public GameObject playerPrefab;
    private GameObject spawnPoint;

    public GameObject cam;

    public int[] tempLevelSequence;
    public int[] tempBonusLevelSequence;

    public bool debugLevel;
    public bool debugMode;
    public bool debugModeActive = false;

    [Header("Gameplay Data")]
    public float startCountdown = 4f;

    public float startTime = 30f;
    public float time;
    public float timeSpeed = 1f;

    public int keyNumberRequired;
    public int startBonus;

    public bool bonusLevel;
    public int bonusBlocksRemaining;

    public int levelPenalty;

    public int border;

    [Header("Game State")]
    public bool gameStarted = false;
    public bool inPause = false;
    public bool gameOver;

    public static int currentLevel = 1;

    [Header("UI Elements")]
    public GameObject gameInfoCanvas;
    public GameObject gameOverCanvas;
    public GameObject winCanvas;
    public GameObject saveCanvas;
    public GameObject pauseMenuCanvas;
    public GameObject endGameCanvas;

    public GameObject debugCanvas;

    public GameObject levelTab;
    public GameObject timeTab;
    public GameObject scoreTab;
    public GameObject keyTab;
    public GameObject bonusTab;
    public GameObject bonusBlocksTab;
    public GameObject countdown;
    
    public GameObject emptyKeyPrefab;
    public GameObject keyPrefab;

    public GameObject[] bonusSprites;


    void Start()
    {
        instance = this;

        if (debugLevel)
        {
            GetComponent<SaveLoad>().LoadLevel(0);
            startCountdown = 0f;
        }
        else
        {
            if (currentLevel != 1 && (currentLevel - 1) % 5 == 0 && PlayerStats.bonus == 5)     // Launch bonus level
            {
                // Play music
                GameObject.Find("Bonus Stage").GetComponent<AudioSource>().PlayOneShot(bonusMusic);

                bonusLevel = true;
                PlayerStats.bonus = 0;
                GetComponent<SaveLoad>().LoadBonusLevel(tempBonusLevelSequence[(currentLevel - 1) / 5 - 1]);
                currentLevel -= 1;
            }
            else 
            {
                // Play music
                GameObject.Find("Stage").GetComponent<AudioSource>().PlayOneShot(stageMusic);

                GetComponent<SaveLoad>().LoadLevel(tempLevelSequence[currentLevel - 1]);
                bonusLevel = false;
            }
            
        }

        // Spawn the player at start position
        spawnPoint = GameObject.FindGameObjectWithTag("Start");
        Debug.Log("Spawning the player...");
        Instantiate(playerPrefab, spawnPoint.transform.position + 0.05f * Vector3.up, spawnPoint.transform.rotation);

        // Initialize the camera position
        Debug.Log("Initialize camera position...");
        cam.GetComponent<Camera>().InitializeCamera();

        gameOver = false;
    }

    void Update()
    {
        // If gameOver (loss or win), do nothing else
        if (gameOver)
        {
            return;
        }

        // Enable debugMode if asked
        if (debugMode && Input.GetKeyDown(KeyCode.Return))
        {
            debugModeActive = !debugModeActive;

            if (debugModeActive)
            {
                Debug.Log("Debug Mode On !");
                debugCanvas.SetActive(true);
            }
            else
            {
                Debug.Log("Debug Mode Off !");
                debugCanvas.SetActive(false);
            }
        }

        // Enable debugMode controls
        if (debugModeActive)
        {
            DebugMode();
        }

        // Toggle pause menu with escape key
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Prevent pause menu if game has not started
            if (startCountdown <= 0.0f)
            {
                StartCoroutine(TogglePauseMenu());
            }
        }

        // If inPause, do nothing else
        if (inPause)
        {
            return;
        }

        // Generate the start countdown before starting the game
        if (startCountdown > 0)
        {
            startCountdown -= Time.deltaTime;

            if (startCountdown <= 1)
            {
                Debug.Log("Game started !");
                gameStarted = true;
            }

            return;
        }

        // Call win function if it's a bonus level and there is no remaining non-activated bonus blocks
        if (bonusLevel && bonusBlocksRemaining == 0)
        {
            Win();
        }

        // Call gameover function if the time reach 0
        if (time <= 0)
        {
            Debug.Log("Game Over");
            GameOver();
            return;
        }

        // Decrease the time according to the time spent by a frame and the time speed (1 by default) and update the UI text
        time -= Time.deltaTime * timeSpeed;
        
        timeTab.GetComponentInChildren<Text>().text = string.Format("{0:00}", time);
    }

    public void UpdateScore()
    {
        Debug.Log("Score: " + PlayerStats.score);
        scoreTab.transform.Find("Score").GetComponent<Text>().text = PlayerStats.score.ToString();
    }

    public void UpdateKeys()
    {
        Debug.Log("Keys: " + PlayerStats.keys + "/" + keyNumberRequired);
        
        GameObject keySprite = Instantiate(keyPrefab, new Vector3(18 + (PlayerStats.keys - 1) * 83, 51.3f, 0), Quaternion.identity);
        keySprite.transform.parent = GameObject.Find("GameInfoCanvas").transform.Find("Keys");

        RectTransform sampleKeySprite = GameObject.Find("SampleKeySprite").GetComponent<RectTransform>();
        keySprite.GetComponent<RectTransform>().localScale = sampleKeySprite.localScale;
        keySprite.GetComponent<RectTransform>().localPosition = new Vector2(sampleKeySprite.localPosition.x + (PlayerStats.keys - 1) * 83, sampleKeySprite.localPosition.y);
    }

    public void UpdateBonus()
    {
        Debug.Log("Bonus: " + PlayerStats.bonus);

        bonusSprites[PlayerStats.bonus - 1].SetActive(true);
    }

    public void UpdateBonusBlocksRemaining()
    {
        Debug.Log("Blocks Remaining: " + bonusBlocksRemaining);
        bonusBlocksTab.GetComponentInChildren<Text>().text = "Blocks Remaining: " + bonusBlocksRemaining.ToString();
    }

    public void Win()
    {
        // Stop music
        if (!bonusLevel) { GameObject.Find("Stage").GetComponent<AudioSource>().Stop(); }
        else { GameObject.Find("Bonus Stage").GetComponent<AudioSource>().Stop(); }

        // Play sound
        GameObject.Find("Victory").GetComponent<AudioSource>().PlayOneShot(victorySound);

        Debug.Log(" Win !");

        gameOver = true;

        Destroy(GameObject.FindGameObjectWithTag("Player"));

        // Add the level score to the player's total score
        PlayerStats.totalScore += PlayerStats.score;

        // End of the game
        if (currentLevel == tempLevelSequence.Length && PlayerStats.bonus != 5)
        {
            endGameCanvas.SetActive(true);
            endGameCanvas.GetComponent<Animator>().SetTrigger("active_1");
            return;
        }

        // If the player reach the 5th level and hasn't the 5 fruits, ask to save (save thing when leaving a bonus level)
        if (currentLevel % 5 == 0 && PlayerStats.bonus != 5)
        {
            PlayerStats.bonus = 0;
            saveCanvas.SetActive(true);
            saveCanvas.GetComponent<Animator>().SetBool("active", true);

            return;
        }

        winCanvas.SetActive(true);
        winCanvas.GetComponent<Animator>().SetBool("active", true);
    }

    public void GameOver()
    {
        // Stop music
        if (!bonusLevel) { GameObject.Find("Stage").GetComponent<AudioSource>().Stop(); }
        else { GameObject.Find("Bonus Stage").GetComponent<AudioSource>().Stop(); }

        // Play sound
        GameObject.Find("Game Over").GetComponent<AudioSource>().PlayOneShot(gameOverSound);

        Debug.Log("GameOver !");

        gameOver = true;

        Destroy(GameObject.FindGameObjectWithTag("Player"));

        // Substract the level score plus the level penalty to the player's total score
        PlayerStats.totalScore -= (PlayerStats.score + levelPenalty);

        // Set the number of bonus of the player to the same with which he started the level
        PlayerStats.bonus = startBonus;

        // If totalscore reach 0, the player can't retry and go to main menu
        if (PlayerStats.totalScore <= 0)
        {
            // Reset the stats
            PlayerStats.totalScore = 0;
            PlayerStats.bonus = 0;

            // Go to main menu
            foreach (Button button in gameOverCanvas.GetComponentsInChildren<Button>())
            {
                button.gameObject.SetActive(false);
            }

            gameOverCanvas.transform.Find("Buttons").Find("Score 0").gameObject.SetActive(true);

            gameOverCanvas.SetActive(true);

            StartCoroutine(GoToMainMenu());
        }

        gameOverCanvas.SetActive(true);
        gameOverCanvas.GetComponent<Animator>().SetBool("active", true);
    }

    public void StartSunglassesBonus()
    {
        StartCoroutine(SunglassesBonus());
    }

    IEnumerator SunglassesBonus()
    {
        Debug.Log("Sunglasses Bonus ON !");

        GameObject[] invisibleBlocks = GameObject.FindGameObjectsWithTag("InvisibleBlock");

        // Enable the mesh renderer of all invisible blocks
        foreach (GameObject block in invisibleBlocks)
        {
            block.GetComponent<MeshRenderer>().enabled = true;
        }

        // Wait 5 seconds
        yield return new WaitForSeconds(5f);

        Debug.Log("Sunglasses Bonus OFF !");

        // Disable the mesh renderer of all invisible blocks
        foreach (GameObject block in invisibleBlocks)
        {
            block.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    IEnumerator GoToMainMenu()
    {
        yield return new WaitForSeconds(4f);

        transitionController.SetTrigger("end");

        yield return new WaitForSeconds(0.5f);

        SceneManager.LoadScene("MainMenu");
    }

    void DebugMode()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            Win();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            Debug.Log("DEBUG MODE: +1 bonus");

            PlayerStats.bonus += 1;
            UpdateBonus();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("DEBUG MODE: +10 score");

            PlayerStats.score += 10;
            UpdateScore();
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            Debug.Log("DEBUG MODE: +10 totalScore");

            PlayerStats.totalScore += 10;
            scoreTab.transform.Find("TotalScore").GetComponent<Text>().text = PlayerStats.totalScore.ToString();
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            Debug.Log("DEBUG MODE: +1 key");

            PlayerStats.keys += 1;
            UpdateKeys();
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            Debug.Log("DEBUG MODE: +10 seconds");

            time += 10;
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            Debug.Log("DEBUG MODE: -1 second");

            time -= 1;
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            if (GameObject.Find("DebugCanvas").GetComponentInChildren<Text>().color.r == 1.0f)
            {
                Debug.Log("DEBUG MODE: Invicibility OFF");
                GameObject.Find("DebugCanvas").GetComponentInChildren<Text>().color = new Color(0.2f, 0.2f, 0.2f, 0.6f);
            }
            else
            {
                Debug.Log("DEBUG MODE: Invicibility ON");
                GameObject.Find("DebugCanvas").GetComponentInChildren<Text>().color = new Color(1f, 0f, 0f, 1f);
            }

            string[] enemyTags = { "Laser", "Captivator", "StandardSpikes", "RetractingSpikes", "LaserSwitch" };

            foreach (string tag in enemyTags)
            {
                GameObject[] enemies = GameObject.FindGameObjectsWithTag(tag);

                foreach (GameObject enemy in enemies)
                {
                    enemy.GetComponentInChildren<BoxCollider>().enabled = !enemy.GetComponentInChildren<BoxCollider>().enabled;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            PlayerStats.LogStats();
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            Win();
            currentLevel = 14;
        }
    }

    public IEnumerator TogglePauseMenu()
    {
        if (inPause)
        {
            pauseMenuCanvas.GetComponent<Animator>().SetBool("active", !inPause);

            // Time flows again in The World
            Time.timeScale = 1;

            yield return new WaitForSeconds(0.92f);

            inPause = !inPause;
            Debug.Log("Pause Menu " + ((inPause) ? "enabled" : "disabled"));

            pauseMenuCanvas.SetActive(false);
            gameInfoCanvas.SetActive(!inPause);
        }
        else
        {

            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>()._isStopping = true;
            // Time is stopped in The World
            Time.timeScale = 0;
            inPause = !inPause;
            Debug.Log("Pause Menu " + ((inPause) ? "enabled" : "disabled"));



            gameInfoCanvas.SetActive(!inPause);

            pauseMenuCanvas.SetActive(true);
            pauseMenuCanvas.GetComponent<Animator>().SetBool("active", inPause);
        }
    }
}
