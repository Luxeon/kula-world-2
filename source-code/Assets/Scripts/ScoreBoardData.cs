﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

[System.Serializable]
public class ScoreBoardData {

    public string[][] scoreBoard;

    public ScoreBoardData(string username)
    {
        string scoreBoardPath = Application.persistentDataPath + "/scoreboard.bin";

        if (File.Exists(scoreBoardPath))
        {
            string[][] oldScoreBoard = SaveLoad.LoadScoreData().scoreBoard;

            scoreBoard = new string[oldScoreBoard.Length + 1][];

            int rank = oldScoreBoard.Length;

            for (int i = 0; i < oldScoreBoard.Length; i++)
            {
                if (PlayerStats.totalScore >= Int32.Parse(oldScoreBoard[i][2]))
                {
                    rank = i;

                    break;
                }
            }

            int j = 0;

            for (int i = 0; i < scoreBoard.Length; i++)
            {
                scoreBoard[i] = new string[3];

                if (i == rank)
                {
                    scoreBoard[i][0] = (i + 1).ToString();
                    scoreBoard[i][1] = username;
                    scoreBoard[i][2] = PlayerStats.totalScore.ToString();
                }
                else
                {
                    if (Int32.Parse(oldScoreBoard[j][2]) < PlayerStats.totalScore)
                    {
                        scoreBoard[i][0] = (Int32.Parse(oldScoreBoard[j][0]) + 1).ToString();
                    }
                    else
                    {
                        scoreBoard[i][0] = oldScoreBoard[j][0];
                    }

                    scoreBoard[i][1] = oldScoreBoard[j][1];
                    scoreBoard[i][2] = oldScoreBoard[j][2];

                    j++;
                }
            }

            return;
        }



        scoreBoard = new string[1][];
        scoreBoard[0] = new string[3];
        scoreBoard[0][0] = "1";
        scoreBoard[0][1] = username;
        scoreBoard[0][2] = PlayerStats.totalScore.ToString();
    }
}
