﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Captivator : MonoBehaviour {

    public GameObject point1;
    public GameObject point2;
    public GameObject captivator;

    public float maxSpeed;
    private float speed;
    private GameObject target;

    public AnimationCurve curve;

    private float maxDistance;

    public float distanceDelta;

    void Start()
    {
        maxDistance = Vector3.Distance(point1.transform.position, point2.transform.position);

        captivator.transform.position = point1.transform.position;

        target = point2;
    }

    void Update ()
    {
        // Move the captivator between point1 and point2
        Vector3 direction = (target.transform.position - captivator.transform.position).normalized;
        float distance = Mathf.Clamp(Vector3.Distance(target.transform.position, captivator.transform.position), 0, maxDistance);

        if (distance <= 0.4f)
        {
            speed = curve.Evaluate(Mathf.Clamp(distance, 0.35f, 0.4f) / 0.4f) * maxSpeed;
        }
        else if (maxDistance - distance <= 0.5f)
        {
            speed = curve.Evaluate(Mathf.Clamp(maxDistance - distance, 0.35f, 0.4f) / 0.4f) * maxSpeed;
        }
        else
        {
            speed = maxSpeed;
        }

        captivator.transform.Translate(direction * speed * Time.deltaTime, Space.World);

        if (distance <= distanceDelta)
        {
            if (target == point1)
            {
                target = point2;
            }
            else
            {
                target = point1;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        // Call GameOver function when hit
        GameManager.instance.GameOver();
    }
}
