﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LevelCreator : MonoBehaviour {

    public int id;
    public bool overrideLevelFile;
    
    public bool load;
    public bool save;
    public bool bonus;

    public int time = 30;
    public int penalty = 20;

    [Header("Level Generation Prefabs")]
    public GameObject startPrefab;
    public GameObject endPrefab;
    public GameObject[] blocksPrefab;
    public GameObject[] itemsPrefab;
    public GameObject[] spikesPrefab;
    public GameObject captivatorPrefab;
    public GameObject laserPrefab;
    public GameObject laserSwitchPrefab;

    void Start()
    {
        id = PlayerPrefs.GetInt("LevelID", 1);
    }

    void Update()
    {
        string folder;

        if (bonus)
        {
            folder = "/BonusLevels";
        }
        else
        {
            folder = "/StandardLevels";
        }

        if (save)
        {
            save = false;

            if (File.Exists(Application.persistentDataPath + "/Levels" + folder + "/level" + id + ".bin"))
            {
                if (!overrideLevelFile)
                {
                    return;
                }
            }

            SaveLoad.SaveLevel(id, time, penalty, bonus);

            if (File.Exists(Application.persistentDataPath + "/Levels" + folder + "/level" + id + ".bin"))
            {
                if (!overrideLevelFile)
                {
                    if (bonus)
                    {
                        PlayerPrefs.SetInt("BonusLevelID", id + 1);
                    }
                    else
                    {
                        PlayerPrefs.SetInt("LevelID", id + 1);
                    }
                }
                else
                {
                    overrideLevelFile = false;
                }
            }
        }

        if (load)
        {
            DestroyLevel();
            LoadLevel(id);
            load = false;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawWireCube(new Vector3(15, 15, 15), new Vector3(27, 27, 27));
    }

    void DestroyLevel()
    {
        string[] tags = new string[] { "Block", "InvisibleBlock", "FlimsyBlock", "BonusBlock", "Key", "Coin", "Gem", "Fruit", "LethargyPill", "BouncyPill", "Hourglass", "Sunglasses", "StandardSpikes", "RetractingSpikes", "Laser", "LaserSwitch", "Captivator", "Start", "End"};

        foreach (string tag in tags)
        {
            GameObject[] objects = GameObject.FindGameObjectsWithTag(tag);

            foreach (GameObject obj in objects)
            {
                Destroy(obj);
            }
        }
    }

    void LoadLevel(int id)
    {
        Debug.Log("Level " + id + " Generation Started !");

        LevelData level = SaveLoad.LoadLevelData(id, bonus);

        float[] start = level.startPosition;
        float[] end = level.endPosition;

        float[][][] blocks = level.blocks;
        string[] blocksFolders = new string[] { "StandardBlocks", "InvisibleBlocks", "FlimsyBlocks", "BonusBlocks" };

        float[][][] items = level.items;
        string[] itemsFolders = new string[] { "Keys", "Hourglasses", "Fruits", "Coins", "Gems", "LethargyPills", "BouncyPills", "Sunglasses" };

        float[][][] spikes = level.spikes;
        string[] spikesFolders = new string[] { "StandardSpikes", "RetractingSpikes" };

        float[][][] captivators = level.captivators;

        float[][][] lasers = level.lasers;

        float[][] laserSwitches = level.laserSwitches;

        // Generate start and end position
        GameObject startGO = Instantiate(startPrefab, new Vector3(start[0], start[1], start[2]), Quaternion.identity);
        startGO.transform.parent = GameObject.Find("Level").transform;

        if (!bonus)
        {
            GameObject endGO = Instantiate(endPrefab, new Vector3(end[0], end[1], end[2]), new Quaternion(end[3], end[4], end[5], end[6]));
            endGO.transform.parent = GameObject.Find("Level").transform;
        }

        // Generate blocks
        for (int i = 0; i < blocks.Length; i++)
        {
            for (int j = 0; j < blocks[i].Length; j++)
            {
                GameObject blockGO = Instantiate(blocksPrefab[i], new Vector3(blocks[i][j][0], blocks[i][j][1], blocks[i][j][2]), Quaternion.identity);
                blockGO.transform.parent = GameObject.Find(blocksFolders[i]).transform;
            }
        }

        // Generate items
        for (int i = 0; i < items.Length; i++)
        {
            for (int j = 0; j < items[i].Length; j++)
            {
                GameObject itemGO = Instantiate(itemsPrefab[i], new Vector3(items[i][j][0], items[i][j][1], items[i][j][2]), new Quaternion(items[i][j][3], items[i][j][4], items[i][j][5], items[i][j][6]));
                itemGO.transform.parent = GameObject.Find(itemsFolders[i]).transform;
            }
        }

        // Generate spikes
        for (int i = 0; i < spikes.Length; i++)
        {
            for (int j = 0; j < spikes[i].Length; j++)
            {
                GameObject spikesGO = Instantiate(spikesPrefab[i], new Vector3(spikes[i][j][0], spikes[i][j][1], spikes[i][j][2]), new Quaternion(spikes[i][j][3], spikes[i][j][4], spikes[i][j][5], spikes[i][j][6]));
                spikesGO.transform.parent = GameObject.Find(spikesFolders[i]).transform;
            }
        }

        // Generate captivators
        for (int i = 0; i < captivators.Length; i++)
        {
            GameObject captivatorGO = Instantiate(captivatorPrefab, new Vector3(captivators[i][0][0], captivators[i][0][1], captivators[i][0][2]), Quaternion.identity);

            captivatorGO.GetComponentInChildren<Captivator>().point2.transform.position = new Vector3(captivators[i][1][0], captivators[i][1][1], captivators[i][1][2]);

            captivatorGO.transform.parent = GameObject.Find("Captivators").transform;
        }

        // Generate lasers
        for (int i = 0; i < lasers.Length; i++)
        {
            GameObject laserGO = Instantiate(laserPrefab, new Vector3(lasers[i][0][0], lasers[i][0][1], lasers[i][0][2]), new Quaternion(lasers[i][0][3], lasers[i][0][4], lasers[i][0][5], lasers[i][0][6]));

            laserGO.GetComponent<Laser>().point2.transform.position = new Vector3(lasers[i][1][0], lasers[i][1][1], lasers[i][1][2]);

            laserGO.GetComponent<Laser>().id = (int)lasers[i][2][0];

            laserGO.GetComponent<Laser>().Initialize();

            laserGO.transform.parent = GameObject.Find("Lasers").transform;
        }

        // Generate laser switches
        for (int i = 0; i < laserSwitches.Length; i++)
        {
            GameObject laserSwitchGO = Instantiate(laserSwitchPrefab, new Vector3(laserSwitches[i][0], laserSwitches[i][1], laserSwitches[i][2]), new Quaternion(laserSwitches[i][3], laserSwitches[i][4], laserSwitches[i][5], laserSwitches[i][6]));

            laserSwitchGO.GetComponent<LaserSwitch>().laserId = (int)laserSwitches[i][7];

            laserSwitchGO.GetComponent<LaserSwitch>().activated = (laserSwitches[i][8] == 1) ? true : false;

            laserSwitchGO.GetComponent<LaserSwitch>().Initialize();

            laserSwitchGO.transform.parent = GameObject.Find("LaserSwitches").transform;
        }

        time = level.time;

        penalty = level.penalty;

        Debug.Log("Level Generation Ended !");
    }
}
