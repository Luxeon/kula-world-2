﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public Button continueButton;
    public Animator animatorController;

    void Awake()
    {
        if (PlayerPrefs.GetInt("Level", 0) == 0)
        {
            continueButton.interactable = false;
        }
    }

    public void NewGame()
    {
        Debug.Log("MainMenu: New Game");

        // Initialize stats variables
        PlayerPrefs.SetInt("Level", 0);
        PlayerStats.totalScore = 0;
        PlayerPrefs.SetInt("TotalScore", 0);
        PlayerStats.bonus = 0;
        GameManager.currentLevel = 1;

        // Transition
        StartCoroutine(StartGameTransition());
    }

    public void Continue()
    {
        Debug.Log("MainMenu: Continue");

        // Initialize stats variables
        PlayerStats.totalScore = PlayerPrefs.GetInt("TotalScore");
        GameManager.currentLevel = PlayerPrefs.GetInt("Level");
        PlayerStats.bonus = 0;

        // Loading the level scene
        Debug.Log("Loading Game Scene...");

        // Transition
        StartCoroutine(StartGameTransition());
    }

    public void Online()
    {
        Debug.Log("MainMenu: Online");
    }

    public void Modify()
    {
        Debug.Log("MainMenu: Modify");

        // Transition
        StartCoroutine(StartModifyTransition());
    }

    public void Quit()
    {
        Debug.Log("MainMenu: Quit");
        Application.Quit();
    }

    IEnumerator StartGameTransition()
    {
        // UI Transition
        animatorController.SetTrigger("end");

        yield return new WaitForSeconds(0.5f);

        // Loading the game scene
        Debug.Log("Loading Game Scene...");
        SceneManager.LoadScene("Game");
    }

    IEnumerator StartModifyTransition()
    {
        // UI Transition
        animatorController.SetTrigger("end");

        yield return new WaitForSeconds(0.5f);

        // Loading the level editor scene
        Debug.Log("Loading Level Editor Scene...");
        SceneManager.LoadScene("LevelEditor");
    }
}
