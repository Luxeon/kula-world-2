﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pill : MonoBehaviour
{
    public int id;
    public GameObject particlesPrefab;
    public AudioClip sound;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // Play sound
            GameObject.Find("Collectible").GetComponent<AudioSource>().PlayOneShot(sound);

            Debug.Log(this.name + " Picked Up !");

            Player player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

            // Start the effect according to the id 
            if (id == 0)
            {
                player.StartPillMalus();
            }
            else if (id == 1)
            {
                player.StartPillBonus();
            }

            Destroy(this.gameObject);

            StartCoroutine(InstantiateParticles());
        }
    }

    IEnumerator InstantiateParticles()
    {
        GameObject particles = Instantiate(particlesPrefab, transform.position + 0.85f * transform.up, transform.rotation);

        yield return new WaitForSeconds(1f);

        Destroy(particles);
    }
}
