﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleBlock : MonoBehaviour {

    void Awake()
    {
        // Disable the mesh renderer and load the material according to the current world
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blocks/World" + (1 + (GameManager.currentLevel - 1) / 5) + "/InvisibleBlock");
    }
}
