﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGate : MonoBehaviour {

    public bool open;

    void Start()
    {
        // Initialize as inactive
        open = false;

        Renderer rend = GetComponentInChildren<Renderer>();
        rend.material.SetColor("_Color", new Color(0.87f, 0.24f, 0.24f));
    }

	void OnTriggerEnter(Collider other)
    {
        // Test if the gate is open (ie all keys are picked up) before call win function
        if (other.tag == "Player" && open)
        {
            GameManager.instance.Win();
        }
    }

    public void CheckIfOpen()
    {
        // Became open if the player picked up all the keys 
        if (PlayerStats.keys == GameManager.instance.keyNumberRequired)
        {
            Debug.Log("EndGate open !");
            open = true;

            Renderer rend = GetComponentInChildren<Renderer>();
            rend.material.SetColor("_Color", new Color (0.32f, 0.93f, 0.24f));
        }
    }
}
