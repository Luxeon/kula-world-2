﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class LevelEditor : MonoBehaviour
{
    // Animator for transitions
    public Animator transitionController;

    // Level object which contains every level-related object
    public GameObject levelObj;

    // Objects to place
    public GameObject blockObj;
    public GameObject bonusBlockObj;
    public GameObject flimsyBlockObj;
    public GameObject invisibleBlockObj;
    public GameObject bouncyPillObj;
    public GameObject lethargyPillObj;
    public GameObject coinObj;
    public GameObject gemObj;
    public GameObject fruitObj;
    public GameObject hourglassObj;
    public GameObject keyObj;
    public GameObject sunglassesObj;
    public GameObject laserObj;
    public GameObject laserSwitchObj;
    public GameObject captivatorObj;
    public GameObject retractingSpikesObj;
    public GameObject standardSpikesObj;
    public GameObject startObj;
    public GameObject endObj;

    // Currently selected object, which will be placed when placing an object
    public GameObject selectedObj;

    public LayerMask layermask;

    public GameObject eraseCheckmark;
    public Button finishButton;

    private bool eraseMode = false;
    private bool isLaserSelected = false;
    private Vector3 selectedLaserFirstPoint;
    private bool isCaptivatorSelected = false;
    private Vector3 selectedCaptivatorFirstPoint;

    // -1 means laserId, time and levelNumber are not valid
    private int laserId = -1;
    private int time = -1;
    private int levelNumber = -1;
    private bool isBonusLevel = false;

    // Start is called before the first frame update
    void Start()
    {
        // Select Block
        ChangeSelectedObj("Block");

        // Assigning default values to input fields
        GameObject.Find("LaserIdInputField").GetComponent<InputField>().text = "0";
        GameObject.Find("TimeInputField").GetComponent<InputField>().text = "30";
        GameObject.Find("LevelNumberInputField").GetComponent<InputField>().text = "1";

        // Show controls in Logs
        ShowDefaultLog();

        // Making invisible objects visible for editor visibility and activating colliders
        //ChangeInvisibleObjectsRender(true);
        //ChangeNonCollidableObjectsColliders(true);
    }

    // Update is called once per frame
    void Update()
    {
        // Verifying if the Level can be finished
        VerifyValidateFinish();

        // Switch toggle Erase Mode by pressing "R"
        if (Input.GetKeyDown(KeyCode.R))
        {
            eraseCheckmark.GetComponent<Toggle>().isOn = !eraseCheckmark.GetComponent<Toggle>().isOn;
        }

        // Change Selected Object to blockObj by pressing "B"
        if (Input.GetKeyDown(KeyCode.B)) { ChangeSelectedObj("Block"); }

        // Change Selected Object to startObj by pressing "S"
        if (Input.GetKeyDown(KeyCode.S)) { ChangeSelectedObj("Start"); }

        // Change Selected Object to endObj by pressing "E"
        if (Input.GetKeyDown(KeyCode.E)) { ChangeSelectedObj("End"); }

        // Change Selected Object to fruitObj by pressing "F"
        if (Input.GetKeyDown(KeyCode.F)) { ChangeSelectedObj("Fruit"); }

        // Change Selected Object to keyObj by pressing "K"
        if (Input.GetKeyDown(KeyCode.K)) { ChangeSelectedObj("Key"); }

        // Change Selected Object to laserObj by pressing "L"
        if (Input.GetKeyDown(KeyCode.L)) { ChangeSelectedObj("Laser"); }
        
        // Change Selected Object to captivatorObj by pressing "C"
        if (Input.GetKeyDown(KeyCode.C)) { ChangeSelectedObj("Captivator"); }

        // Getting the RaycastHit
        // This will allow to get the clicked object
        RaycastHit hit;
        Ray ray = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);

        // If the left mouse button is clicked and an object is clicked
        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 100, layermask))
        {
            // Get the clicked object
            GameObject hitObj = GetGameObjectFromHit(hit);

            // If Alt is being held, change the selected object
            if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
            {
                //Debug.Log(hitObj.tag);
                if (hitObj.tag == "Laser") { ChangeObjectLaserId(hitObj.GetComponent<Laser>().id); }
                else if (hitObj.tag == "LaserSwitch") { ChangeObjectLaserId(hitObj.GetComponent<LaserSwitch>().laserId); }
                ChangeSelectedObj(hitObj.tag);
            } else
            {
                //Debug.Log(GetNewTransformPosition(hit.transform.position, hit.point));
                //Debug.Log(hitObj.name);
                string tagType = GetTagType(hitObj.tag);

                // If Erase Mode is enabled
                if (eraseMode)
                {
                    // When erasing a block, check if another block will remain
                    if (tagType != "Block" || CheckIsMoreThanOneBlockInScene())
                    {
                        // Erase the cliked object
                        EraseObject(hitObj);
                    }
                    else { ShowLog("You can't erase this block. At least one block should remain."); }
                }
                // Checking if the object can be placed
                // Spikes cannot be placed on an invisible block
                else if (tagType == "Block")
                {
                    // Placing an object
                    // hit.point is the position where the cursor (from the click) has landed
                    VerifyAndPlaceObject(selectedObj, hit.transform.gameObject, hit.point);
                }
            }
        }
    }

    // Disable all effects enabled for level editor and start the Game scene
    // Called when clicking the finish button
    public void Finish()
    {
        // Make invisible objects invisible (such as invisible blocks and start)
        ChangeInvisibleObjectsRender(false);
        // Restore objects colliders
        ChangeNonCollidableObjectsColliders(false);

        // During the game, the player might glitch when he is in a negative position
        // To prevent this, we move all level-related objects to positive positions
        MoveLevelObjectsToPositivePositions();

        // Save Level
        GetComponent<SaveLoad>().SaveLevelFromEditor(levelNumber, time, isBonusLevel);

        ChangeSceneToMenu();
    }

    public void EditExistingLevel()
    {
        // Erase every level-related object

        // Destroying start first because the next start will be used
        //DestroyImmediate(GameObject.FindGameObjectWithTag("Start"));

        // Getting every object in the scene
        GameObject[] objects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        // Checking every object
        foreach (GameObject obj in objects)
        {
            // Checking if the object is level-related
            if (GetTagType(obj.tag) != "None")
            {
                if (obj.tag == "Start")
                {
                    Debug.Log("oui" + obj.name);
                }
                // Erasing the object
                DestroyImmediate(obj);
            }
        }
        
        // Load the level
        GetComponent<SaveLoad>().LoadLevelFromEditor(levelNumber, isBonusLevel);
        
        // Getting the new start
        GameObject start = GameObject.FindGameObjectWithTag("Start");


        
        // Making invisible objects visible for editor visibility and activating colliders
        ChangeInvisibleObjectsRender(true);
        ChangeNonCollidableObjectsColliders(true);

        // Moving camera on start
        if (CheckIsObjectInScene(start))
        {
            GameObject.Find("Main Camera").GetComponent<LevelEditorCamera>().ChangeLookAtPosition(start.transform.position);
            Debug.Log("camera" + start.transform.position);
        }

        // Show controls in Logs
        ShowDefaultLog();
    }

    public void ChangeSceneToMenu()
    {
        StartCoroutine(TransitionToMenu());
    }

    // Verify if the finish button is interactable
    // Called when clicking the finish button (even if non-interactable)
    public void VerifyFinishInteractable()
    {
        // If the button is not interactable
        if (!finishButton.interactable)
        {
            if (!isBonusLevel)
            {
                ShowLog("Start, End, Key and Crystal must be placed. Input fields must be filled.");
            } else
            {
                ShowLog("In bonus mode, one Bonus Block should be present, but no End, Key or Crystal");
            }
        }
    }

    // Move the level to a a positive position ( > 0 )
    private void MoveLevelObjectsToPositivePositions()
    {
        // Searching the minimal position in the scene
        float minPosition = GetMinObjectPosition();

        // Checking if level should be moved
        // We check if minPosition is less than 2 instead of 0 to reduce risks
        if (minPosition < 2)
        {
            // Get the positive value from minPosition
            int distance = (int)Math.Ceiling(Math.Abs(minPosition)) + 2;
            Vector3 distanceVector = new Vector3(distance, distance, distance);

            // Move level-related objects

            // Getting every object in the scene
            GameObject[] objects = UnityEngine.Object.FindObjectsOfType<GameObject>();

            // Checking every object
            foreach (GameObject obj in objects)
            {
                // Checking if the object is level-related
                if (GetTagType(obj.tag) != "None")
                {
                    // Getting the object position
                    Vector3 objPosition = obj.transform.position;

                    // Moving the object
                    MoveObject(obj, objPosition + distanceVector);
                }
            }
        }
    }

    // Move an object to another position in the scene
    private void MoveObject(GameObject obj, Vector3 position)
    {
        obj.transform.position = position;
    }

    // Get the minimal x, y or z position in the scene
    private float GetMinObjectPosition()
    {
        // Getting every object in the scene
        GameObject[] objects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        float minPosition = GetObjCoordinates(objects[0]).x;

        // Checking every object
        foreach (GameObject obj in objects)
        {
            // Checking if the object is level-related
            if (GetTagType(obj.tag) != "None")
            {
                // Getting the minimal position for every axis
                if (minPosition > GetObjCoordinates(obj).x) { minPosition = GetObjCoordinates(obj).x; }
                if (minPosition > GetObjCoordinates(obj).y) { minPosition = GetObjCoordinates(obj).y; }
                if (minPosition > GetObjCoordinates(obj).z) { minPosition = GetObjCoordinates(obj).z; }
            }
        }

        return minPosition;
    }

    // Change the visuals of all invisible objects (whether it is visible or invisible)
    // Concerns invisible blocks and start
    private void ChangeInvisibleObjectsRender(bool isVisible)
    {
        // Getting every single invisible block in the scene
        GameObject[] invisibleBlocks = GameObject.FindGameObjectsWithTag("InvisibleBlock");

        // Changing the mesh renderer of every single invisible block
        foreach (GameObject block in invisibleBlocks)
        {
            block.GetComponent<MeshRenderer>().enabled = isVisible;
        }

        GameObject start = GameObject.FindGameObjectWithTag("Start");

        // Changing the mesh renderer of the start object
        start.GetComponentInChildren<MeshRenderer>().enabled = isVisible;

    }

    // Change the collider of objects who aren't supposed to be collidable in game
    private void ChangeNonCollidableObjectsColliders(bool isCollidable)
    {
        // Change lasers points colliders for every laser
        GameObject[] lasers = GameObject.FindGameObjectsWithTag("Laser");

        foreach (GameObject laser in lasers)
        {
            ChangeLaserPointsColliders(laser.transform.Find("Point1").gameObject, laser.transform.Find("Point2").gameObject, true);
        }

        // Change laser switches colliders for every laser switch
        GameObject[] laserSwitches = GameObject.FindGameObjectsWithTag("LaserSwitch");

        foreach (GameObject laserSwitch in laserSwitches)
        {
            ChangeLaserSwitchCollider(laserSwitch, isCollidable);
        }

        GameObject start = GameObject.FindGameObjectWithTag("Start");
        // Changing the collisions of the start object (needed to make the start object clickable)
        start.GetComponentInChildren<BoxCollider>().enabled = isCollidable;
    }

    private void ChangeLaserPointsColliders(GameObject point1, GameObject point2, bool isCollidable)
    {
        if (isCollidable)
        {
            // Add points colliders
            point1.AddComponent<BoxCollider>();
            point2.AddComponent<BoxCollider>();

            /*
            // Adjust colliders sizes
            point1.GetComponent<BoxCollider>().center = point1.transform.localPosition;
            //point1.GetComponent<BoxCollider>().size = new Vector3();
            point2.GetComponent<BoxCollider>().center = point2.transform.localPosition;
            //point2.GetComponent<BoxCollider>().size = new Vector3(0.3f, 0.3f, 0.3f);*/
        } else
        {
            Destroy(point1.GetComponent<BoxCollider>());
            Destroy(point2.GetComponent<BoxCollider>());
        }

    }

    // Change laser switch collider size to fit model when isSizeChanged = true
    private void ChangeLaserSwitchCollider(GameObject laserSwitch, bool isSizeChanged)
    {
        BoxCollider laserSwitchBoxCollider = laserSwitch.GetComponent<BoxCollider>();
        if (isSizeChanged)
        {
            laserSwitchBoxCollider.size = new Vector3(0.2f, 0.1f, 0.2f);
            laserSwitchBoxCollider.center = new Vector3(0f, -0.45f, 0f);
        } else  // Default in game values
        {
            laserSwitchBoxCollider.size = laserSwitchObj.GetComponent<BoxCollider>().size;
            laserSwitchBoxCollider.center = laserSwitchObj.GetComponent<BoxCollider>().center;
        }
    }

    // Verify if the level contains start and end so the level can be finished
    public bool VerifyValidateFinish()
    {
        // time and levelNumber are not valid if their values equals -1
        bool isTimeValid = (time != -1);
        bool isLevelNumberValid = (levelNumber != -1);
        bool areObjectsPresent = (CheckIsObjectInScene(startObj) && CheckIsObjectInScene(endObj) 
            && CheckIsObjectInScene(keyObj) && CheckIsObjectInScene(fruitObj));
        bool areObjectsPresentBonus = (CheckIsObjectInScene(startObj) && !CheckIsObjectInScene(endObj)
            && !CheckIsObjectInScene(keyObj) && !CheckIsObjectInScene(fruitObj));

        if (!isBonusLevel)
        {
            // Check if the scene contains a Start, End, Key and Fruit, and if time and levelNumber are valid.
            if (areObjectsPresent && isTimeValid && isLevelNumberValid)
            {
                finishButton.interactable = true;
                return true;
            }
        } else
        {
            if (areObjectsPresentBonus && isTimeValid && isLevelNumberValid)
            {
                finishButton.interactable = true;
                return true;
            }
        }
        finishButton.interactable = false;
        return false;
    }

    // Change the selected object, that will be placed by clicking
    // Called on objects dropdown change
    public void ChangeSelectedObj(int index)
    {
        switch (index)
        {
            case 0: selectedObj = blockObj; break;
            case 1: selectedObj = bonusBlockObj; break;
            case 2: selectedObj = flimsyBlockObj; break;
            case 3: selectedObj = invisibleBlockObj; break;
            case 4: selectedObj = bouncyPillObj; break;
            case 5: selectedObj = lethargyPillObj; break;
            case 6: selectedObj = coinObj; break;
            case 7: selectedObj = gemObj; break;
            case 8: selectedObj = fruitObj; break;
            case 9: selectedObj = hourglassObj; break;
            case 10: selectedObj = keyObj; break;
            case 11: selectedObj = sunglassesObj; break;
            case 12: selectedObj = laserObj; break;
            case 13: selectedObj = laserSwitchObj; break;
            case 14: selectedObj = captivatorObj; break;
            case 15: selectedObj = retractingSpikesObj; break;
            case 16: selectedObj = standardSpikesObj; break;
            case 17: selectedObj = startObj; break;
            case 18: selectedObj = endObj; break;
        }

        // If the selected object has changed, reset selected laser and captivator
        if (index != 12) { DeselectLaser(); }
        if (index != 14) { DeselectCaptivator(); }
    }

    // Overload method, change the selected object, from tag
    public void ChangeSelectedObj(string tag)
    {
        Dropdown dropdown = GameObject.Find("ObjectsDropdown").GetComponent<Dropdown>();

        switch (tag)
        {
            case "Block": dropdown.value = 0; break;
            case "BonusBlock": dropdown.value = 1; break;
            case "FlimsyBlock": dropdown.value = 2; break;
            case "InvisibleBlock": dropdown.value = 3; break;
            case "BouncyPill": dropdown.value = 4; break;
            case "LethargyPill": dropdown.value = 5; break;
            case "Coin": dropdown.value = 6; break;
            case "Gem": dropdown.value = 7; break;
            case "Fruit": dropdown.value = 8; break;
            case "Hourglass": dropdown.value = 9; break;
            case "Key": dropdown.value = 10; break;
            case "Sunglasses": dropdown.value = 11; break;
            case "Laser": dropdown.value = 12; break;
            case "LaserSwitch": dropdown.value = 13; break;
            case "Captivator": dropdown.value = 14; break;
            case "RetractingSpikes": dropdown.value = 15; break;
            case "StandardSpikes": dropdown.value = 16; break;
            case "Start": dropdown.value = 17; break;
            case "End": dropdown.value = 18; break;
        }

        // If the selected object has changed, reset selected laser and captivator
        if (tag != "Laser") { DeselectLaser(); }
        if (tag != "Captivator") { DeselectCaptivator(); }
    }

    // Change the erase mode to true or false
    // This is called by the "Erase Mode" UI Toggle
    public void ChangeEraseMode(bool checkmark)
    {
        eraseMode = checkmark;

        // If the erase mode has changed, reset selected laser and captivator
        DeselectLaser();
        DeselectCaptivator();
    }

    // Verify if the Time entered is valid
    // This is called by the "Time" UI Input Field
    public void VerifyLaserIdInputField(string laserIdString)
    {
        int id = 0;
        int maxIdNumber = 5;

        // If laserIdString isn't valid or if id isn't between 0 and maxIdNumber, then result is invalid
        // id is lasterIdString converted to int
        if (!Int32.TryParse(laserIdString, out id) || !(id >= 0 && id <= maxIdNumber))
        {
            // -1 means it is not valid
            laserId = -1;
            ShowLog("Laser Id should be an integer between 0 and " + maxIdNumber + ".");
        }
        else  // 
        {
            laserId = id;
            ShowLog("Laser Id is valid.");
        }
    }

    private void ChangeObjectLaserId(int id)
    {
        GameObject.Find("LaserIdInputField").GetComponent<InputField>().text = id.ToString();
    }

    // Verify if the Time entered is valid
    // This is called by the "Time" UI Input Field
    public void VerifyTimeInputField(string timeString)
    {
        int i = 0;
        // If timeString isn't valid or if i isn't positive, then result is invalid
        // i is timeString converted to int
        if (!Int32.TryParse(timeString, out i) || i < 1)
        {
            // -1 means it is not valid
            time = -1;
            ShowLog("Time value should be a positive integer.");
        } else
        {
            time = i;
            ShowLog("Time value is valid.");
        }
    }

    // Verify if the Level entered is valid
    // This is called by the "LevelNumber" UI Input Field
    public void VerifyLevelInputField(string levelString)
    {
        int i = 0;
        int maxLevelNumber = 15;
        
        if (isBonusLevel) { maxLevelNumber = 3; }

        // If levelString isn't valid or if i isn't between 0 and maxLevelNumber, then result is invalid
        // i is levelString converted to int
        if (!Int32.TryParse(levelString, out i) || !(i >= 1 && i <= maxLevelNumber))
        {
            // -1 means it is not valid
            levelNumber = -1;
            ShowLog("Level number should be an integer between 1 and " + maxLevelNumber + ".");
            // Level can't be edited, hide button and which level to edit
            ShowLevelOnEditButton();
        } else
        {
            levelNumber = i;
            // Level can be edited, show button and which level to edit
            ShowLevelOnEditButton();
            ShowDefaultLog();
        }
    }

    // Change isBonusLevel to true or false
    // This is called by the "Is a Bonus Level" UI Toggle
    public void ChangeIsBonusLevel(bool checkmark)
    {
        isBonusLevel = checkmark;

        // Number of normal levels and bonus level are not equals
        // Verify again to prevent entering an invalid value
        VerifyLevelInputField(GameObject.Find("LevelNumberInputField").GetComponent<InputField>().text);
        // Verify again to prevent finishing with bad finish conditions
        VerifyValidateFinish();
    }

    // Place the object in the correct "folder"
    // All level-related objects need to be children of the level object
    private void ChangeObjParent(GameObject obj)
    {
        Transform parent = levelObj.transform;

        switch (GetTagType(obj.tag))
        {
            case "Block":
                parent = parent.GetChild(0);
                switch (obj.tag)
                {
                    case "Block": parent = parent.GetChild(0); break;
                    case "InvisibleBlock": parent = parent.GetChild(1); break;
                    case "FlimsyBlock": parent = parent.GetChild(2); break;
                    case "BonusBlock": parent = parent.GetChild(3); break;
                } break;
            case "Item":
                parent = parent.GetChild(1);
                switch (obj.tag)
                {
                    case "Key": parent = parent.GetChild(0); break;
                    case "Hourglass": parent = parent.GetChild(1); break;
                    case "Fruit": parent = parent.GetChild(2); break;
                    case "Coin": parent = parent.GetChild(3); break;
                    case "Gem": parent = parent.GetChild(4); break;
                    case "Sunglasses": parent = parent.GetChild(5); break;
                    case "LethargyPill": parent = parent.GetChild(6); break;
                    case "BouncyPill": parent = parent.GetChild(7); break;
                } break;
            case "Obstacle":
                parent = parent.GetChild(2);
                switch (obj.tag)
                {
                    case "StandardSpikes": parent = parent.GetChild(0); break;
                    case "RetractingSpikes": parent = parent.GetChild(1); break;
                    case "Captivator": parent = parent.GetChild(2); break;
                    case "Laser": parent = parent.GetChild(3); break;
                    case "LaserSwitch": parent = parent.GetChild(4); break;
                } break;
        }

        //Debug.Log(parent);
        obj.transform.SetParent(parent);
    }

    // Get the position of the hitpoint relatively to the clicked object
    private Vector3 GetRelativeHitPoint(Vector3 originalTransformPosition, Vector3 hitPointPosition)
    {
        return hitPointPosition - originalTransformPosition;
    }

    // Get the position of the object that will be placed
    private Vector3 GetNewTransformPosition(Vector3 originalTransformPosition, Vector3 hitPoint)
    {
        Vector3 relativeHitPoint = GetRelativeHitPoint(originalTransformPosition, hitPoint);

        if (CheckIsApproxEqual(relativeHitPoint.x, 0.5))  // Return value even if value approximately equals
        {
            // This means the hitPoint has landed on x front side of the clicked block
            // The position of the future object will be the be same position of the original block + 1 on its side
            return new Vector3(originalTransformPosition.x + 1, originalTransformPosition.y, originalTransformPosition.z);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.y, 0.5))
        {
            return new Vector3(originalTransformPosition.x, originalTransformPosition.y + 1, originalTransformPosition.z);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.z, 0.5))
        {
            return new Vector3(originalTransformPosition.x, originalTransformPosition.y, originalTransformPosition.z + 1);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.x, -0.5))
        {
            return new Vector3(originalTransformPosition.x - 1, originalTransformPosition.y, originalTransformPosition.z);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.y, -0.5))
        {
            return new Vector3(originalTransformPosition.x, originalTransformPosition.y - 1, originalTransformPosition.z);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.z, -0.5))
        {
            return new Vector3(originalTransformPosition.x, originalTransformPosition.y, originalTransformPosition.z - 1);
        }
        else  // This is an error case, it should never happen
        {
            return new Vector3();
        }
    }

    // Get the rotation of the object that will be placed
    private Quaternion GetNewTransformRotation(Vector3 originalTransformPosition, Vector3 hitPoint)
    {
        Vector3 relativeHitPoint = GetRelativeHitPoint(originalTransformPosition, hitPoint);

        if (CheckIsApproxEqual(relativeHitPoint.x, 0.5))
        {
            // Quaternion.Euler is a rotation Vector based on degrees angles
            return Quaternion.Euler(0, 0, -90);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.y, 0.5))
        {
            return Quaternion.Euler(0, 0, 0);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.z, 0.5))
        {
            return Quaternion.Euler(90, 0, 0);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.x, -0.5))
        {
            return Quaternion.Euler(0, 0, 90);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.y, -0.5))
        {
            return Quaternion.Euler(-180, 0, 0);
        }
        else if (CheckIsApproxEqual(relativeHitPoint.z, -0.5))
        {
            return Quaternion.Euler(-90, 0, 0);
        }
        else
        {
            // Don't change the rotation (the original rotation of the object will remain)
            return Quaternion.identity;
        }
    }

    // Put an object in the scene
    private void VerifyAndPlaceObject(GameObject obj, GameObject originalBlock, Vector3 hitPoint)
    {
        Vector3 originalPosition = originalBlock.transform.position;
        Quaternion objRotation = GetNewTransformRotation(originalPosition, hitPoint);
        Vector3 objPosition = GetNewTransformPosition(originalPosition, hitPoint);

        //Debug.Log(originalPosition);
        //Debug.Log(objPosition);
        if (CheckIsPlaceable(obj, objPosition, objRotation, originalBlock))
        {
            switch (GetTagType(obj.tag))
            {
                case "Block":
                    PlaceBlock(obj, objPosition);
                    break;
                // Items and Obstacles need to be placed on the original position
                case "Item":
                    if (obj.tag == "Fruit") { PlaceFruit(obj, originalPosition, objRotation); }
                    else { PlaceRotatedObject(obj, originalPosition, objRotation); }
                    break;
                case "Obstacle":
                    switch (obj.tag)
                    {
                        case "Captivator": PlaceCaptivator(obj, objPosition); break;
                        case "Laser": PlaceLaser(obj, objPosition); break;
                        case "LaserSwitch": PlaceLaserSwitch(obj, objPosition, objRotation); break;
                        default: PlaceRotatedObject(obj, originalPosition, objRotation); break;
                    }
                    break;
                case "Unique":
                    if (obj.tag == "Start") { PlaceStart(obj, objPosition, objRotation); }
                    else if (obj.tag == "End") { PlaceEnd(obj, originalPosition, objRotation); }
                    else { PlaceRotatedObject(obj, originalPosition, objRotation); }
                    break;
            }
        }
    }

    private void PlaceBlock(GameObject block, Vector3 blockPosition)
    {
        // Placing the block in the scene
        block = Instantiate(block, blockPosition, Quaternion.identity);
        // Changing the object parent for hierarchy visibility
        ChangeObjParent(block);

        // When placing an invisble block
        if (block.tag == "InvisibleBlock")
        {
            // Making the block visible
            block.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    private void PlaceRotatedObject(GameObject obstacle, Vector3 obstaclePosition, Quaternion obstacleRotation)
    {
        // Placing the object in the scene
        obstacle = Instantiate(obstacle, obstaclePosition, obstacleRotation);
        ChangeObjParent(obstacle);
    }

    private void PlaceStart(GameObject start, Vector3 startPosition, Quaternion startRotation)
    {
        // Removing the previous start, the level can only have one start
        GameObject prvStart = GameObject.FindGameObjectWithTag("Start");
        if (prvStart != null) { EraseObject(prvStart); }

        start = Instantiate(start, startPosition, startRotation);
        ChangeObjParent(start);
        // Making the start object invisible
        start.GetComponentInChildren<MeshRenderer>().enabled = true;
        // Activating collisions on start (needed to make the start object clickable)
        start.GetComponentInChildren<BoxCollider>().enabled = true;
    }

    private void PlaceEnd(GameObject end, Vector3 endPosition, Quaternion endRotation)
    {
        // Removing the previous end, the level can only have one end
        GameObject prvEnd = GameObject.FindGameObjectWithTag("End");
        if (prvEnd != null) { EraseObject(prvEnd); }

        end = Instantiate(end, endPosition, endRotation);
        ChangeObjParent(end);
    }

        private void PlaceFruit(GameObject fruit, Vector3 fruitPosition, Quaternion fruitRotation)
    {
        // Removing the previous start, the level can only have one start
        GameObject prvFruit = GameObject.FindGameObjectWithTag("Fruit");
        if (prvFruit != null) { EraseObject(prvFruit); }

        fruit = Instantiate(fruit, fruitPosition, fruitRotation);
        ChangeObjParent(fruit);
    }

    private void PlaceLaser(GameObject laser, Vector3 laserPosition)
    {
        // If a laser is not already being placed, we place one
        if (!isLaserSelected)
        {
            // We select the laser first point
            selectedLaserFirstPoint = laserPosition;
            isLaserSelected = true;
            ShowLog("You placed the first laser point, click again to place the second one.");
        }
        else  // A laser is being placed
        {
            // Generating a temporary clone to assign him values before instantiating the actual laser
            GameObject laserTemp = Instantiate(laser, selectedLaserFirstPoint, Quaternion.identity);
            // Changing the second point of the laser
            laserTemp.GetComponentInChildren<Laser>().point2.transform.position = laserPosition;
            // Changing the id of the laser
            laserTemp.GetComponent<Laser>().id = laserId;

            // Placing the actual laser
            laser = Instantiate(laserTemp, selectedLaserFirstPoint, Quaternion.identity);

            // Destroying the temporary clone
            Destroy(laserTemp);

            // Changing laser collider size for level editor purpose
            ChangeLaserPointsColliders(laser.transform.Find("Point1").gameObject, laser.transform.Find("Point2").gameObject, true);
            
            ChangeObjParent(laser);

            // Reset the selected laser
            DeselectLaser();
        }
    }

    private void PlaceLaserSwitch(GameObject laserSwitch, Vector3 laserSwitchPosition, Quaternion laserSwitchRotation)
    {
        // Generating a temporary clone to assign him values before instantiating the actual laser switch
        GameObject laserSwitchTemp = Instantiate(laserSwitch, laserSwitchPosition, laserSwitchRotation);

        // Changing the id of the laser switch
        laserSwitchTemp.GetComponent<LaserSwitch>().laserId = laserId;
        
        // Changing in editor collider size
        ChangeLaserSwitchCollider(laserSwitchTemp, true);

        // Placing the actual laser switch
        laserSwitch = Instantiate(laserSwitchTemp, laserSwitchPosition, laserSwitchRotation);

        // Destroying the temporary clone
        Destroy(laserSwitchTemp);

        ChangeObjParent(laserSwitch);
    }

    private void PlaceCaptivator(GameObject captivator, Vector3 captivatorPosition)
    {
        // If a captivator is not already being placed, we place one
        if (!isCaptivatorSelected)
        {
            // We select the captivator first point
            selectedCaptivatorFirstPoint = captivatorPosition;
            isCaptivatorSelected = true;
            ShowLog("You placed the first captivator point, click again to place the second one.");
        } else  // A captivator is being placed
        {
            captivator = Instantiate(captivator, selectedCaptivatorFirstPoint, Quaternion.identity);
            ChangeObjParent(captivator);
            // Change the second point of the captivator
            captivator.GetComponentInChildren<Captivator>().point2.transform.position = captivatorPosition;
            // Reset the selected captivator
            DeselectCaptivator();
        }
    }

    // Erase an object in the scene
    private void EraseObject(GameObject objToErase)
    {
        // Checking if the object is a block
        if (GetTagType(objToErase.tag) == "Block")
        {
            // Erase every obstacle on this block
            EraseObstaclesOnBlock(objToErase);
        }

        // Delete the object from the scene
        Destroy(objToErase);
    }
    
    // Erase obstacles such as spikes or ends
    private void EraseObstaclesOnBlock(GameObject block)
    {
        Vector3 blockPosition = block.transform.position;
        GameObject[] laserSwitches = GameObject.FindGameObjectsWithTag("LaserSwitch");
        GameObject[] standardSpikes = GameObject.FindGameObjectsWithTag("StandardSpikes");
        GameObject[] retractingSpikes = GameObject.FindGameObjectsWithTag("RetractingSpikes");
        GameObject start = GameObject.FindGameObjectWithTag("Start");
        GameObject[] ends = GameObject.FindGameObjectsWithTag("End");

        foreach (GameObject laserSwitch in laserSwitches)
        {
            // We check if the laserSwitch is on the the top of the block
            if (laserSwitch.transform.position == GetUpperPosition(blockPosition, laserSwitch.transform.rotation))
            {
                EraseObject(laserSwitch);
            }
        }

        foreach (GameObject spikes in standardSpikes)
        {
            // We check if the spikes and the block have the same position because when this kind of obstacle is placed on a block, they have the same position
            if (spikes.transform.position == blockPosition)
            {
                EraseObject(spikes);
            }
        }

        foreach (GameObject spikes in retractingSpikes)
        {
            // We check if the spikes and the block have the same position because when this kind of obstacle is placed on a block, they have the same position
            if (spikes.transform.position == blockPosition)
            {
                EraseObject(spikes);
            }
        }

        if (start != null)
        {
            if (start.transform.position == GetUpperPosition(blockPosition, start.transform.rotation))
            {
                EraseObject(start);
            }
        }

        foreach (GameObject end in ends)
        {
            // We check if the end obstacle and the block have the same position because when this kind of obstacle is placed on a block, they have the same position
            if (end.transform.position == blockPosition)
            {
                EraseObject(end);
            }
        }
    }

    private bool CheckIsPlaceable(GameObject objToPlace, Vector3 position, Quaternion rotation, GameObject originalBlock)
    {
        // If spikes are being placed on an invisible block or flimsy block
        if ((originalBlock.tag == "FlimsyBlock" || originalBlock.tag == "InvisibleBlock") && (objToPlace.tag == "StandardSpikes" || objToPlace.tag == "RetractingSpikes"))
        {
            // Spikes are not placeable
            return false;
        }

        // If obstacles placed on Flimsy block or an invisible block
        if ((originalBlock.tag == "FlimsyBlock" || originalBlock.tag == "InvisibleBlock") && (objToPlace.tag == "Start" || objToPlace.tag == "End" || objToPlace.tag == "LaserSwitch"))
        {
            // Not placeable
            return false;
        }

        bool isLaserIdValid = (laserId != -1);

        switch (objToPlace.tag)  // Object being placed
        {
            case "Laser":
                if (!isLaserIdValid)  // If laserId isn't valid
                {
                    ShowLog("You can't place the laser because laserId isn't valid.");
                    // Object can't be place
                    return false;
                }
                else if ((isLaserSelected && !CheckArePositionsFreelyAligned(selectedLaserFirstPoint, position)))  // If laser first and second points are not freely aligned
                {
                    ShowLog("Laser points must be aligned and have no occupied place in the path.");
                    // Object can't be place
                    return false;
                }
                break;
            case "LaserSwitch":
                if (!isLaserIdValid)  // If laserId isn't valid
                {
                    ShowLog("You can't place the laser switch because laserId isn't valid.");
                    // Object can't be place
                    return false;
                }
                break;
            case "Captivator":
                // If captivator first and second points are not freely aligned
                if (isCaptivatorSelected && !CheckArePositionsFreelyAligned(selectedCaptivatorFirstPoint, position))
                {
                    ShowLog("Captivator points must be aligned and have no occupied place in the path.");
                    // Object can't be place
                    return false;
                }
                break;
        }

        // Object is placeable if the place is not occupied
        return !CheckIsPlaceOccupied(position);
    }

    private bool CheckIsPlaceOccupied(Vector3 posToPlace)
    {
        if (CheckIsPositionWithinCaptivatorsZone(posToPlace))
        {
            return true;
        }

        if (CheckIsPositionWithinLasersZone(posToPlace))
        {
            return true;
        }

        GameObject[] objects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        //Debug.Log("posToPlace: " + posToPlace);

        foreach (GameObject obj in objects)
        {
            // Objects might contain unrelated gameObjects
            // Verifying if they are level-related
            if (GetTagType(obj.tag) != "None")
            {
                if (posToPlace == GetObjCoordinates(obj))
                {
                    return true;
                }
                //Debug.Log(obj.tag + ": " + GetObjCoordinates(obj));
            }
        }
        return false;
    }

    // Check if the object is present in the level editor
    private bool CheckIsObjectInScene(GameObject searchedObj)
    {
        GameObject[] objects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        foreach (GameObject obj in objects)
        {
            if (obj.tag == searchedObj.tag)
            {
                    return true;
            }
        }

        return false;
    }

    // Check if more than one block is remaining in the scene
    private bool CheckIsMoreThanOneBlockInScene()
    {
        GameObject[] objects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        int numberOfBlocks = 0;

        foreach (GameObject obj in objects)
        {
            if (GetTagType(obj.tag) == "Block")
            {
                numberOfBlocks++;
            }
        }

        if (numberOfBlocks > 1) { return true; }
        else { return false; }
    }

    // Check if 2 positions are aligned and if there is an occupied place between them
    private bool CheckArePositionsFreelyAligned(Vector3 position1, Vector3 position2)
    {
        //Debug.Log(position1);
        //Debug.Log(position2);
        if (position1 == position2) { return true; } // Same position
        if (position1.y == position2.y && position1.z == position2.z)  // Aligned on x
        { return !CheckArePlacesOccupiedInTheWayOfAxis("x", position1, position2); }
        if (position1.x == position2.x && position1.z == position2.z)  // Aligned on y
        { return !CheckArePlacesOccupiedInTheWayOfAxis("y", position1, position2); }
        if (position1.x == position2.x && position1.y == position2.y)  // Aligned on z
        { return !CheckArePlacesOccupiedInTheWayOfAxis("z", position1, position2); }
        return false;  // Not aligned
    }

    // Check if a place is occupied between two aligned positions
    private bool CheckArePlacesOccupiedInTheWayOfAxis(string positionAxis, Vector3 position1, Vector3 position2)
    {
        int pos1Axis = 0;
        int pos2Axis = 0;

        switch (positionAxis)
        {
            case "x":
                // int positions on x axis
                pos1Axis = (int)Math.Round(position1.x);
                pos2Axis = (int)Math.Round(position2.x);
                break;
            case "y":
                // int positions on y axis
                pos1Axis = (int)Math.Round(position1.y);
                pos2Axis = (int)Math.Round(position2.y);
                break;
            case "z":
                // int positions on z axis
                pos1Axis = (int)Math.Round(position1.z);
                pos2Axis = (int)Math.Round(position2.z);
                break;
        }


        while (pos1Axis < pos2Axis)
        {
            pos1Axis++;
            if (CheckIsPlaceOccupied(GetNewPositionWithAxis(positionAxis, pos1Axis, position1))) { return true; }
            // Check if every positions between the two positions are free

            if (pos1Axis == pos2Axis) { return false; }
        }
        while (pos1Axis > pos2Axis)
        {
            pos2Axis++;
            if (CheckIsPlaceOccupied(GetNewPositionWithAxis(positionAxis, pos2Axis, position2))) { return true; }

            // Check if every positions between the two positions are free
            if (pos1Axis == pos2Axis) { return false; }
        }

        // Every positions between the two positions are free
        // Should not be accessible, done because all code paths need to return a value
        return false;
    }

    // Check if position is in the zone of lasers
    private bool CheckIsPositionWithinLasersZone(Vector3 position)
    {
        GameObject[] lasers = GameObject.FindGameObjectsWithTag("Laser");

        foreach (GameObject laser in lasers)
        {
            if (CheckIsObjectPresentInAxisObjZone(position, laser))
            {
                ShowLog("This position is in the path of a laser.");
                return true;
            }
        }

        return false;
    }

    // Check if position is in the zone of captivators
    private bool CheckIsPositionWithinCaptivatorsZone(Vector3 position)
    {
        GameObject[] captivators = GameObject.FindGameObjectsWithTag("Captivator");

        foreach (GameObject captivator in captivators)
        {
            if (CheckIsObjectPresentInAxisObjZone(position, captivator))
            {
                ShowLog("This position is in the path of a captivator.");
                return true;
            }
        }

        return false;
    }

    // Check if an object is in the zone of a captivator or a laser
    private bool CheckIsObjectPresentInAxisObjZone(Vector3 objPosition, GameObject axisObj)
    {
        Vector3 position1;
        Vector3 position2;

        if (axisObj.tag == "Captivator")
        {
            position1 = axisObj.GetComponentInChildren<Captivator>().point1.transform.position;
            position2 = axisObj.GetComponentInChildren<Captivator>().point2.transform.position;
        } else if (axisObj.tag == "Laser")
        {
            position1 = axisObj.GetComponentInChildren<Laser>().point1.transform.position;
            position2 = axisObj.GetComponentInChildren<Laser>().point2.transform.position;
        } else { return false; }

        if (position1 == position2) { return false; } // Same position
        if (position1.y == position2.y && position1.z == position2.z)  // Aligned on x
        { return CheckIsPositionInTheWayOfAxis(objPosition, "x", position1, position2); }
        if (position1.x == position2.x && position1.z == position2.z)  // Aligned on y
        { return CheckIsPositionInTheWayOfAxis(objPosition, "y", position1, position2); }
        if (position1.x == position2.x && position1.y == position2.y)  // Aligned on z
        { return CheckIsPositionInTheWayOfAxis(objPosition, "z", position1, position2); }
        return false;  // Not aligned
    }

    // Check if a specified object is present between two aligned positions
    private bool CheckIsPositionInTheWayOfAxis(Vector3 objPosition, string positionAxis, Vector3 position1, Vector3 position2)
    {
        int pos1Axis = 0;
        int pos2Axis = 0;

        switch (positionAxis)
        {
            case "x":
                // int positions on x axis
                pos1Axis = (int)Math.Round(position1.x);
                pos2Axis = (int)Math.Round(position2.x);
                break;
            case "y":
                // int positions on y axis
                pos1Axis = (int)Math.Round(position1.y);
                pos2Axis = (int)Math.Round(position2.y);
                break;
            case "z":
                // int positions on z axis
                pos1Axis = (int)Math.Round(position1.z);
                pos2Axis = (int)Math.Round(position2.z);
                break;
        }

        while (pos1Axis <= pos2Axis)
        {
            if (objPosition == GetNewPositionWithAxis(positionAxis, pos1Axis, position1)) { return true; }

            // Check if every positions between the two positions are free
            if (pos1Axis == pos2Axis) { return false; }

            pos1Axis++;
        }
        while (pos1Axis >= pos2Axis)
        {
            if (objPosition == GetNewPositionWithAxis(positionAxis, pos2Axis, position2)) { return true; }

            // Check if every positions between the two positions are free
            if (pos1Axis == pos2Axis) { return false; }

            pos2Axis++;
        }

        // Should not be accessible, done because all code paths need to return a value
        return false;
    }

    // return if value1 approximatively equals value2
    private bool CheckIsApproxEqual(double value1, double value2)
    {
        double approximationPercent = 0.01;
        return (value1 - value2 < approximationPercent && value1 - value2 > -approximationPercent);
    }

    // Get the position of an object
    private Vector3 GetObjCoordinates(GameObject obj)
    {
        if (GetTagType(obj.tag) == "Item" || obj.tag == "RetractingSpikes" || obj.tag == "StandardSpikes" || obj.tag == "End")
        {
            return GetUpperPosition(obj.transform.position, obj.transform.rotation);
        }
        return obj.transform.position;
    }

    // Get category of an object
    private string GetTagType(string tag)
    {
        switch (tag)
        {
            case "Block": return "Block";
            case "BonusBlock": return "Block";
            case "FlimsyBlock": return "Block";
            case "InvisibleBlock": return "Block";
            case "Fruit": return "Item";
            case "BouncyPill": return "Item";
            case "LethargyPill": return "Item";
            case "Coin": return "Item";
            case "Gem": return "Item";
            case "Hourglass": return "Item";
            case "Key": return "Item";
            case "Sunglasses": return "Item";
            case "Laser": return "Obstacle";
            case "LaserSwitch": return "Obstacle";
            case "Captivator": return "Obstacle";
            case "RetractingSpikes": return "Obstacle";
            case "StandardSpikes": return "Obstacle";
            case "Start": return "Unique";
            case "End": return "Unique";
            default: return "None";  // Should not be accessible for a level-related object
        }
    }

    // Get the actual object when hitted with RaycastHit
    private GameObject GetGameObjectFromHit(RaycastHit hit)
    {
        GameObject obj = hit.transform.gameObject;

        string name = obj.name;
        GameObject parent = obj.transform.parent.gameObject;

        // Clicked obj might be a GameObject child, in this case we need to get the parent
        if (name == "Start" || name == "End" || parent.tag == "Laser" || name == "Captivator" || parent.tag == "RetractingSpikes")
        {
            obj = obj.transform.parent.gameObject;
        }
        return obj;
    }

    // Get the position of an object one block higher
    // Some objects such as spikes takes position where the block is placed, we may need to get their upper position
    private Vector3 GetUpperPosition(Vector3 position, Quaternion rotation)
    {
        // We shouldn't work with quaternion values, we take eulerAngles which correspond to degrees angles 
        Vector3 eulerRotation = rotation.eulerAngles;

        if (CheckIsApproxEqual(eulerRotation.x, 0))
        {
            if (CheckIsApproxEqual(eulerRotation.z, 270)) // facing x
            {
                position.x += 1;
            }
            else if (CheckIsApproxEqual(eulerRotation.z, 90)) // facing -x
            {
                position.x -= 1;
            }
            else if (CheckIsApproxEqual(eulerRotation.z, 0)) // facing upward (y)
            {
                position.y += 1;
            }
            else if (CheckIsApproxEqual(eulerRotation.z, 180)) // facing downward (-y)
            {
                position.y -= 1;
            }
        }
        else if (CheckIsApproxEqual(eulerRotation.z, 0))
        {
            if (CheckIsApproxEqual(eulerRotation.x, 90)) // facing z
            {
                position.z += 1;
            }
            else if (CheckIsApproxEqual(eulerRotation.x, 270)) // facing -z
            {
                position.z -= 1;
            }
        }
        else // Should not be accessible
        {
            //Debug.Log("Erreur");
            //Debug.Log(eulerRotation);
        }
        return position;
    }

    private Vector3 GetNewPositionWithAxis(string positionAxis, int axisValue, Vector3 position)
    {
        switch (positionAxis)
        {
            case "x":
                position.x = axisValue;
                break;
            case "y":
                position.y = axisValue;
                break;
            case "z":
                position.z = axisValue;
                break;
        }
        return position;
    }

    private void DeselectLaser()
    {
        ShowDefaultLog();
        isLaserSelected = false;
    }

    private void DeselectCaptivator()
    {
        ShowDefaultLog();
        isCaptivatorSelected = false;
    }

    // Verify if the laser id selection display should be active or not
    // Called on objects dropdown change
    public void VerifyIsLaserSelected(int index)
    {
        // If laser or laserSwitch selected
        if (index == 12 || index == 13) { ChangeLaserIdSelectionDisplay(true); }
        else { ChangeLaserIdSelectionDisplay(false); }
    }

    // Change the visibility of the laser id input field and text
    public void ChangeLaserIdSelectionDisplay(bool isVisible)
    {
        GameObject LaserIdInputField = GameObject.Find("LaserIdInputField");
        LaserIdInputField.GetComponent<InputField>().interactable = isVisible;
        LaserIdInputField.transform.Find("Text").GetComponent<Text>().enabled = isVisible;
        LaserIdInputField.GetComponent<InputField>().placeholder.enabled = isVisible;
        // Prevent placeholder from showing above text
        if (!(LaserIdInputField.GetComponent<InputField>().text == ""))
        {
            LaserIdInputField.GetComponent<InputField>().placeholder.enabled = false;
        }

        GameObject.Find("LaserIdLabelText").GetComponentInChildren<Text>().enabled = isVisible;
    }
    
    // Show on the Edit Existing Level Button which level is selected
    public void ShowLevelOnEditButton()
    {
        if (levelNumber != -1)
        {
            // Change text and make button interactable
            GameObject.Find("EditExistingLevelButton").GetComponentInChildren<Text>().text = "Edit Existing Level " + levelNumber;
            GameObject.Find("EditExistingLevelButton").GetComponent<Button>().interactable = true;
        } else
        {
            // Reset text and hide button
            GameObject.Find("EditExistingLevelButton").GetComponentInChildren<Text>().text = "Edit Existing Level";
            GameObject.Find("EditExistingLevelButton").GetComponent<Button>().interactable = false;
        }
    }

    // Show a log in the UI Logs box
    public void ShowLog(string message)
    {
        GameObject.Find("LogText").GetComponent<Text>().text = message;
    }

    // Show controls in Logs
    public void ShowDefaultLog()
    {
        ShowLog("Place objects by clicking on a block. Move camera with middle click and right click.");
    }

    // Go to the Main Menu
    IEnumerator TransitionToMenu()
    {
        transitionController.SetTrigger("end");

        yield return new WaitForSeconds(0.5f);
        
        SceneManager.LoadScene("MainMenu");
    }
}
