﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	public void Retry()
    {
        Debug.Log("GameOver: Retry");

        SceneManager.LoadScene("Game");
    }

    public void Menu()
    {
        Debug.Log("GameOver: Menu");

        SceneManager.LoadScene("MainMenu");
    }
}
