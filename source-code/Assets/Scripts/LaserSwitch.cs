﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSwitch : MonoBehaviour {

    public bool activated = false;
    public int laserId;
    public AudioClip sound;

    void Start()
    {
        Initialize();
    }

	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // Play sound
            GameObject.Find("Click").GetComponent<AudioSource>().PlayOneShot(sound);

            // Toggle the switch when hit
            activated = !activated;
            Debug.Log(this.name + ((activated) ? " On" : " Off"));

            if (activated)
            {
                On();
            }
            else
            {
                Off();
            }
        }
    }

    void On()
    {
        transform.position -= transform.up * 0.08f;
        ToggleLaser();
    }

    void Off()
    {
        transform.position += transform.up * 0.08f;
        ToggleLaser();
    }

    void ToggleLaser()
    {
        // Toggle the laser line renderer and collider according to the switch state
        GameObject[] lasers = GameObject.FindGameObjectsWithTag("Laser");

        foreach (GameObject laser in lasers)
        {
            Laser l = laser.GetComponent<Laser>();

            if (l.id == laserId)
            {
                l.laser.enabled = !activated;
                laser.GetComponent<BoxCollider>().enabled = !activated;
            }
        }
    }

    public void Initialize()
    {
        // Initialize the material according to the id 
        GetComponentInChildren<MeshRenderer>().material = Resources.Load<Material>("Materials/Obstacles/Laser/Laser" + laserId);

        if (activated)
        {
            On();
        }
    }
}
