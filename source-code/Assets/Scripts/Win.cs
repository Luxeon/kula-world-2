﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour {

    public Animator transitionController;

    public void NextLevel()
    {
        Debug.Log("Win: NextLevel");

        GameManager.currentLevel += 1;

        StartCoroutine(TransitionCoroutine("Game"));
    }

    public void Retry()
    {
        Debug.Log("Win: Retry");

        StartCoroutine(TransitionCoroutine("Game"));
    }

    public void Menu()
    {
        Debug.Log("Win: Menu");

        StartCoroutine(TransitionCoroutine("MainMenu"));
    }

    IEnumerator TransitionCoroutine(string name)
    {
        transitionController.SetTrigger("end");

        yield return new WaitForSeconds(0.5f);        

        SceneManager.LoadScene(name);
    }
}
