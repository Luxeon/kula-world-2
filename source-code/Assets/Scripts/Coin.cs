﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    void Start()
    {
        transform.RotateAround(transform.position, transform.up, Random.Range(0, 360));
    }
}
