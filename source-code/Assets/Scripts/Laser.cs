﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour {

    public LineRenderer laser;
    public GameObject point1;
    public GameObject point2;

    public int id;

	void Awake()
    {
        Initialize();
    }

    void OnTriggerEnter(Collider other)
    {
        // Call GameOver function when hit
        GameManager.instance.GameOver();
    }

    public void Initialize()
    {
        // Initialise the laser and the collider
        laser.SetPosition(0, point1.transform.position);
        laser.SetPosition(1, point2.transform.position);

        laser.enabled = true;

        BoxCollider laserCollider = GetComponent<BoxCollider>();

        Debug.Log((point2.transform.position - point1.transform.position).normalized * Vector3.Distance(point1.transform.position, point2.transform.position) + new Vector3(0.1f, 0.1f, 0.1f));

        Debug.Log((Vector3.Distance(point1.transform.position, point2.transform.position) / 2) * (point2.transform.position - point1.transform.position).normalized);

        laserCollider.size = (point2.transform.position - point1.transform.position).normalized * Vector3.Distance(point1.transform.position, point2.transform.position) + new Vector3(0.1f, 0.1f, 0.1f);
        laserCollider.center = (Vector3.Distance(point1.transform.position, point2.transform.position) / 2) * (point2.transform.position - point1.transform.position).normalized;

        point1.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Obstacles/Laser/Laser" + id);
        point2.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Obstacles/Laser/Laser" + id);
    }
}
