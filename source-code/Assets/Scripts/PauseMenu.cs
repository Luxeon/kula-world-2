﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public Animator transitionController;

    public void Resume()
    {
        Debug.Log("PauseMenu: Continue");

        StartCoroutine(GameManager.instance.TogglePauseMenu());
    }

    public void Retry()
    {
        Debug.Log("PauseMenu: Retry");

        PlayerStats.score = 0;
        PlayerStats.bonus = GameManager.instance.startBonus;

        StartCoroutine(TransitionCoroutine("Game"));
    }

    public void Menu()
    {
        Debug.Log("PauseMenu: Menu");

        StartCoroutine(TransitionCoroutine("MainMenu"));
    }

    IEnumerator TransitionCoroutine(string name)
    {
        // When Time is stopped, Time flows again in The World
        Time.timeScale = 1;

        transitionController.SetTrigger("end");

        yield return new WaitForSeconds(0.5f);

        SceneManager.LoadScene(name);
    }
}
