﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

	void Awake()
    {
        // Load the material corresponding to the current world
        //GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blocks/World" + (1 + (GameManager.currentLevel - 1) / 5) + "/StandardBlock");
        transform.RotateAround(transform.position, transform.up, 90 * Random.Range(0, 4));
        transform.RotateAround(transform.position, transform.right, 90 * Random.Range(0, 4));
        transform.RotateAround(transform.position, transform.forward, 90 * Random.Range(0, 4));
    }
}
