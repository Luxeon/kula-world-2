﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditorCamera : MonoBehaviour
{
    // consts
    private const float Y_ANGLE_MIN = -89.9f;
    private const float Y_ANGLE_MAX = 89.9f;
    private const float DISTANCE_MIN = 1.0f;

    // lookAt is an invisble non-collision object allowing to decide where the camera is looking
    private GameObject lookAt;

    public GameObject Landmark;

    public Transform camTransform;

    // distance between the camera and the lookAt object
    private float distance = 10.0f;

    // same distance for every axis
    //private float distanceDiffX;
    //private float distanceDiffY;
    //private float distanceDiffZ;

    // position of the camera
    private float currentX = 0.0f;
    private float currentY = 0.0f;

    // position of the lookAt object
    private Vector3 lookAtPos = new Vector3(0.0f, 0.0f, 0.0f);

    // scale of the Landmark
    //private float landmarkSize = 0.12f;

    // speed the camera movement (given the action)
    public float sensitivityX = 4.0f;
    public float sensitivityY = 4.0f;
    public float sensitivityZoom = 4.0f;
    public float sensitivityMove = 0.026f;

    // speed of the lookAt Object
    //private float sensitivityLookAt = 0.2f;

    private bool isLookAtMoving = false;


    // Start is called before the first frame update
    private void Start()
    {
        camTransform = transform;

        currentX = 225f;
        currentY = 35f;

        lookAt = Instantiate(Landmark, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));
        lookAt.transform.rotation = Quaternion.Euler(0, 0, 0);
        //LandmarkScale();
    }


    // Update is called once per frame
    private void Update()
    {
        // Changing distance when mouse scrolling
        // Prevent distance from exceeding its limit when zooming
        if (!(distance < DISTANCE_MIN && Input.GetAxis("Mouse ScrollWheel") > 0))
        {
            distance -= Input.GetAxis("Mouse ScrollWheel") * sensitivityZoom;
            //distanceDiffX = camTransform.position.x - lookAt.transform.position.x;
            //distanceDiffY = camTransform.position.y - lookAt.transform.position.y;
            //distanceDiffZ = camTransform.position.z - lookAt.transform.position.z;
        }
        // Another way of preventing distance from exceeding its limit when zooming
        // This is used as an alternative because it might glitch
        //distance = Mathf.Clamp(distance, DISTANCE_MIN, float.MaxValue);

        // If the middle mouse button is held down
        if (Input.GetMouseButton(2) || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            // lookAt is moving
            isLookAtMoving = true;

            // lookAt is moving depending on the camera rotation
            lookAtPos -= (camTransform.right * Input.GetAxis("Mouse X") + camTransform.up * Input.GetAxis("Mouse Y")) * sensitivityMove * (distance + 0.5f);
            lookAt.transform.position = lookAtPos;
        }
        
        // If the right mouse button is held down
        if (Input.GetMouseButton(1) && !Input.GetMouseButton(2))
        {
            currentX += Input.GetAxis("Mouse X") * sensitivityX;
            currentY -= Input.GetAxis("Mouse Y") * sensitivityY;

            // Stopping the angle at top and bottom
            currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
        }
    }

    // LateUpdate is called after all Update functions have been called
    private void LateUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        camTransform.position = lookAt.transform.position + rotation * dir;
        //LandmarkScale();

        // Preventing camera rotation from changing when lookAt moving due to asynchronous movements
        if (!isLookAtMoving)
        {
            camTransform.LookAt(lookAt.transform.position);
        } else
        {
            isLookAtMoving = false;
        }
    }

    public void ChangeLookAtPosition(Vector3 position)
    {
        isLookAtMoving = true;
        lookAt.transform.position = position;
        lookAtPos = position;
    }

    // Change the size of the landmark when zooming
    /* private void LandmarkScale()
    {
        //lookAt.transform.localScale = new Vector3(distance * landmarkSize, distance * landmarkSize, distance * landmarkSize);
        //Landmark.transform.GetChild(0).SetWidth(distance / 1000, distance / 1000);
    } */
}
