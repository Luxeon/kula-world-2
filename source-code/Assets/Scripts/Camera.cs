﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

    private Player player;

    [SerializeField]
    private bool _isRotating;
    [SerializeField]
    private bool _stopRotating;
    [SerializeField]
    private float _step;
    [SerializeField]
    private float _angle;
    [SerializeField]
    private int _clockWise;
    [SerializeField]
    private int _orientation;

    private float _camDistance;

    [SerializeField]
    private bool _autoPosition;

    public float turnSpeed;

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            return;
        }

        transform.position = player.transform.position;
    }

    public void StartRotateCameraCoroutine(float angle, Vector3 axis)
    {
        StartCoroutine(RotateCamera(angle, axis));
    }

    IEnumerator RotateCamera(float angle, Vector3 axis)
    {
        float totalAngle = 0;

        float direction = angle / Mathf.Abs(angle);

        while (totalAngle < 90)
        {
            if (totalAngle + turnSpeed * Time.deltaTime >= 90)
            {
                transform.Rotate(axis, (90 - totalAngle) * direction, Space.World);
            }
            else
            {
                transform.Rotate(axis, direction * turnSpeed * Time.deltaTime, Space.World);
            }

            totalAngle += turnSpeed * Time.deltaTime;

            yield return 0;
        }

        _isRotating = false;

        player._isRotating = false;
    }

    public void InitializeCamera()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        transform.position = player.transform.position + 2.25f * player.axisY - 4.2f * player.axisZ;
        transform.eulerAngles = player.transform.eulerAngles;
        _camDistance = Vector3.Distance(transform.position, player.transform.position);
    }
}
