﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusBlock : MonoBehaviour {

    public bool active;

    void Start()
    {
        // Set the block as inactive and load its "inactive material"
        active = false;
        GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blocks/Bonus/BonusBlockOff");
    }

    void OnCollisionEnter(Collision collision)
    {
        // Become active when the player touch it and update the stats
        if (collision.gameObject.tag == "Player" && !active)
        {
            active = true;
            GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/Blocks/Bonus/BonusBlockOn");
            GameManager.instance.bonusBlocksRemaining -= 1;
            GameManager.instance.UpdateBonusBlocksRemaining();
        }

        return;
    }
}
