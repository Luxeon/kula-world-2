﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour {

    private bool enteringName = true;

    public Text usernameText;
    public GameObject usernameInput;
    public Button validateButton;
    public Animator transitionController;

    public void EnterName()
    {
        enteringName = false;

        SaveLoad.SaveScore(usernameText.text);
        GameManager.instance.GetComponent<SaveLoad>().LoadScoreBoard();

        this.GetComponent<Animator>().SetTrigger("active_2");

        StartCoroutine(ScrollbarInit());
    }

    public void Menu()
    {
        StartCoroutine(TransitionCoroutine("MainMenu"));
    }

    void Update()
    {
        if (enteringName)
        {
            validateButton.interactable = usernameText.text != "";

            if (Input.GetKeyDown(KeyCode.Return) && usernameText.text != "")
            {
                EnterName();
            }
        }
    }

    IEnumerator TransitionCoroutine(string name)
    {
        transitionController.SetTrigger("end");

        yield return new WaitForSeconds(0.5f);

        SceneManager.LoadScene(name);
    }

    IEnumerator ScrollbarInit()
    {
        yield return new WaitForSeconds(0.3f);

        this.GetComponentInChildren<Scrollbar>().value = 1;
    }
}
