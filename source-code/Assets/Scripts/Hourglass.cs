﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hourglass : MonoBehaviour {

    public GameObject particlesPrefab;
    public AudioClip sound;

	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // Play sound
            GameObject.Find("Collectible").GetComponent<AudioSource>().PlayOneShot(sound);

            Debug.Log(this.name + " Picked Up !");

            // Reverse the time 
            GameManager.instance.time = GameManager.instance.startTime - GameManager.instance.time;

            Destroy(this.gameObject);

            StartCoroutine(InstantiateParticles());
        }
    }

    IEnumerator InstantiateParticles()
    {
        GameObject particles = Instantiate(particlesPrefab, transform.position + 0.85f * transform.up, transform.rotation);

        yield return new WaitForSeconds(1f);

        Destroy(particles);
    }
}
