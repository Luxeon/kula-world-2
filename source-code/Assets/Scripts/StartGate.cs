﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGate : MonoBehaviour
{
    void Awake()
    {
        // Disable the mesh renderer
        GetComponent<MeshRenderer>().enabled = false;
    }
}
